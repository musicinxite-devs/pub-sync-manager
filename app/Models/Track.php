<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Track extends Model
{
    protected $hidden = ['pivot'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'file_path', 'album', 'artist'
    ];
	
	public function libraries()
    {
        return $this->belongsToMany('App\Models\Library');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Category');
    }
    /**
     * Get a validator for customer.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validator(array $data, $id = '')
    {
        return Validator::make($data, [
            'title' => 'required|max:255',
            'file_path' => 'required|max:255',
        ]);
    }
}
