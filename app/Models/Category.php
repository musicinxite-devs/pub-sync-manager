<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Category extends Model
{
    public function tracks()
    {
        return $this->belongsToMany('App\Models\Track');
    }

    public function library()
    {
        return $this->hasMany('App\Models\Library');
    }

    public function stream()
    {
        return $this->hasMany('App\Models\Stream');
    }

    public function customer()
    {
        return $this->hasMany('App\Models\Customer');
    }
    /**
     * Get a validator for category.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validator(array $data, $id = '')
    {
        return Validator::make($data, [
            'title' => 'required|max:255',
        ]);
    }

}
