<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TrackRemovalRequest extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 'stream_id', 'meta_data', 'type'
    ];


    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    public function stream()
    {
        return $this->belongsTo('App\Models\Stream');
    }
}
