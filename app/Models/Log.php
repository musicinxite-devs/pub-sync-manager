<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Log extends Model
{
    protected $dates = ['event_datetime'];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'customer_id', 'event', 'event_datetime', 'device_detail','device_model','ios_version'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }
}
