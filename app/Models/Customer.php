<?php

namespace App\Models;

use Validator;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
     protected $dates = ['subscription_end_date', 'subscription_start_date'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'subscription_start_date', 'subscription_end_date'
    ];

    /**
     * Get the category for the customers.
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

	/**
     * Get a validator for customer.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validator(array $data, $id = '')
    {
        return Validator::make($data, [
            'name' => 'required|max:255',
            'pin' => 'digits:4',
            'email' => 'required|email|max:255|unique:customers' . ($id != '' ? ',email,'.$id : ''),
            'removal_request_email' => 'email|max:255',
            'password' => ($id == '' ? 'required|min:6' : ''),
            'subscription_end_date' => 'date|after:subscription_start_date'
        ]);
    }
	
	public function streams()
    {
        return $this->belongsToMany('App\Models\Stream');
    }

    public function log()
    {
        return $this->hasMany('App\Models\Log');
    }

    public function device()
    {
        return $this->hasMany('App\Models\Device');
    }

    public function trackRemovalRequest()
    {
        return $this->hasMany('App\Models\TrackRemovalRequest');
    }

    public function getSubscriptionStartDate($format = '') {
        return ($format != '' && $this->subscription_start_date != null) ?
            $this->subscription_start_date->format($format) :
            $this->subscription_start_date;
    }

    public function getSubscriptionEndDate($format = '') {
        return ($format != '' && $this->subscription_end_date != null) ?
            $this->subscription_end_date->format($format) :
            $this->subscription_end_date;
    }

}
