<?php

namespace App\Models;

use Validator;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	
	 /**
     * Get a validator for user.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validator(array $data, $id = '')
    {
        return Validator::make($data, [
            'name' => 'max:255',
            'email' => 'required|email|max:255|unique:users' . ($id != '' ? ',email,'.$id : ''),
            'password' => ($id == '' ? 'required|confirmed|min:6' : ''),
            'role_id' => 'required'
        ]);
    }

    public function role()
    {
        return $this->belongsTo('App\Models\Role');
    }

    public function customer()
    {
        return $this->hasOne('App\Models\Customer');
    }
}
