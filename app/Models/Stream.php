<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Stream extends Model
{
    protected $hidden = ['pivot'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title', 'url', 'status', 'library_id'
    ];
	
	public function customers()
    {
        return $this->belongsToMany('App\Models\Customer');
    }
	
	public function library()
    {
        return $this->belongsTo('App\Models\Library');
    }

    public function trackRemovalRequest()
    {
        return $this->hasMany('App\Models\TrackRemovalRequest');
    }

    /**
     * Get the category for the stream.
     */
    public function categories()
    {
        return $this->belongsTo('App\Models\Category');
    }
    /**
     * Get a validator for customer.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validator(array $data, $id = '')
    {
        return Validator::make($data, [
            'title' => 'required|max:255',
            'url' => 'required|max:255'
        ]);
    }
}
