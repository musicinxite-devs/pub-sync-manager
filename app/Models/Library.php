<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Validator;

class Library extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'title','category_id', 'status'
    ];
	
	public function streams()
    {
        return $this->hasMany('App\Models\Stream');
    }
	
	public function tracks()
    {
        return $this->belongsToMany('App\Models\Track');
    }

    /**
     * Get the category for the offline library.
     */
    public function category()
    {
        return $this->belongsTo('App\Models\Category');
    }

    /**
     * Get a validator for customer.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validator(array $data, $id = '')
    {
        return Validator::make($data, [
            'title' => 'required|max:255'
        ]);
    }
}
