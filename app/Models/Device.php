<?php

namespace App\Models;

use Validator;
use Illuminate\Database\Eloquent\Model;


class Device extends Model
{

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer');
    }

    /**
     * Get a validator for device.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public static function validator(array $data)
    {
        return Validator::make($data, [
            'uu_id' => 'required',
        ]);
    }
}
