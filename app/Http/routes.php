<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


Route::group(['middlewareGroups' => 'web'], function () {
    Route::auth();
    Route::controller('api', 'ApiController');
    Route::get('track/download/{id}', 'TrackController@getDownload');

    Route::group(['middleware' => ['auth' , 'navigation']], function() {
        Route::get('/', 'DashboardController@index');
        // Only authenticated users may enter...
        Route::controller('dashboard', 'DashboardController');
        Route::controller('user', 'UserController');
        Route::controller('role', 'RoleController');
        Route::controller('customer', 'CustomerController');
        Route::controller('device', 'DeviceController');
        Route::controller('category', 'CategoryController');
        Route::controller('track', 'TrackController');
        Route::controller('library', 'LibraryController');
        Route::controller('stream', 'StreamController');
        Route::controller('account', 'AccountController');
    });
});