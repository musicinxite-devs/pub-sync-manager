<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Category;
use App\Models\Library;
use App\Models\Stream;
use \Illuminate\Http\Request;
use Datatables;
use App\Helpers\Helper;

class StreamController extends Controller
{
    public function getIndex() {
        $categories = Category::where('type','stream')->get()->toArray();
        $str = '';
        $cat_parent = array();
        if($categories) {
            $catArr = Helper::buildTree($categories);
            $str = Helper::createDropDown($catArr,'',0,'',$cat_parent);
        }

        return view('stream.index',compact('str'));
    }

    public function getCreate() {
        $categories = Category::where('type','stream')->where('status','Y')->get()->toArray();
        $libraries = Library::all();
        $cat_parent = array();
        $catArr = array();
        $str = '';
        if($categories) {
            $catArr = Helper::buildTree($categories);
            $str = Helper::createDropDown($catArr,'',0,'',$cat_parent);
        }
        return view('stream.form', compact('libraries','str'));
    }

    public function getEdit($id = '') {
        $categories = Category::where('type','stream')->where('status','Y')->get()->toArray();
        $libraries = Library::all();
        $stream = Stream::findOrFail($id);

        $cat_parent = array();
        $catArr = array();
        $str = '';
        if($categories) {
            $catArr = Helper::buildTree($categories);
            $category_id = $stream->category_id;
            $cat_parent[] = $category_id;
            $str = Helper::createDropDown($catArr,'',0,'',$cat_parent);
        }
        return view('stream.form', compact(
            'stream',
            'libraries',
            'str'
        ));
    }

    public function postSave(Request $request) {
        $id = $request->get("id", '');
        $validator = Stream::validator($request->all(), $id);
        if($validator->fails()) {
            // throw errors
            return redirect()->back()
                ->withErrors($validator->getMessageBag())
                ->withInput($request->all());
        }
        if($id == '') {
            $stream = new Stream();
        } else {
            $stream = Stream::findOrFail($id);
        }
        $stream->title = $request->get("title",'');
        $stream->url = $request->get("url",'');
        $stream->status = $request->get("status",'active');
        $stream->category_id = $request->get("category_id",null);
        $library_id = $request->get("library_id", null);

        if($library_id == null) {
            $stream->library()->dissociate();
        } else {
            $stream->library()->associate(Library::findOrFail($library_id));
        }

        if(!$stream->save()) {
            throw new \Exception(trans('dashboard.unable_to_save_stream'));
        }

        $message = trans('dashboard.dynamic.stream_'.($id == '' ? 'created' : 'updated').'_success');
        return redirect(action('StreamController@getIndex'))->with('success', $message);
    }

    public function postDelete(Request $request) {
        try {
            $id = $request->get("id");
            $stream = Stream::findOrFail($id);
            $exists = $stream->customers()->where('stream_id', $id)->exists();

            if( $exists ){
                return response()->json(['exist' => true]);
            } else {
                $stream->delete();
            }

            return response()->json(['success' => true]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postAnyData(Request $request) {
        //$stream = Stream::select('streams.*');
        $stream = Stream::select('streams.*','categories.title AS category_title')
            ->leftjoin('categories', function ($join) {
                $join->on('streams.category_id', '=', 'categories.id')->where('categories.status','=','Y');
            });
        $datatables = Datatables::of($stream)
            ->addColumn('category_title', function ($stream) {
                return $stream->category_title;
            })
            ->addColumn('action', function ($stream) {
                return '<a href="'. action('StreamController@getEdit', [$stream->id]) .'" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o"></i> Edit</a>&nbsp;' .
                '<a  href="'. action('StreamController@postDelete') . '" data-id="'. $stream->id . '" class="btn btn-xs btn-primary btn-delete"><i class="fa fa-trash"></i> Delete</a>';
            });

            if($request->has('category_id')){
                $datatables->where('category_id', $request->get('category_id',''));
            }
            return $datatables ->make(true);
    }
}
