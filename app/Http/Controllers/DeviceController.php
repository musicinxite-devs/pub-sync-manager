<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Device;
use App\Http\Requests;
use Datatables;

class DeviceController extends Controller
{
    public function getIndex(){
        return view('device.index');
    }

    public function getList($cust_id = ''){
        return view('device.index', compact('cust_id'));
    }

    public function getCreate($cust_id = '') {

        return view('device.form', compact('cust_id'));
    }

    public function getEdit($id = '') {
        $device = Device::findOrFail($id);
        return view('device.form', compact('device'));
    }

    public function postSave(Request $request) {

        try {
            $id = $request->get("id", '');
            $validator = Device::validator($request->all());
            if ($validator->fails()) {
                // throw errors
                return redirect()->back()
                    ->withErrors($validator->getMessageBag())
                    ->withInput($request->all());
            }

            if ($id == '') {
                $device = new Device();
            } else {
                $device = Device::findOrFail($id);
            }
            $device->uu_id = $request->get("uu_id", '');
            $device->customer_id = $request->get("customer_id", '');
            $device->save();
            //$customer->subscription_start_date = $request->get("subscription_start_date");
            return redirect(action('DeviceController@getList',[$device->customer_id]))->with('success');

        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            return redirect()->back()
            ->withErrors($message)
            ->withInput($request->all());
        }
    }

    public function postDelete(Request $request) {
        try {
            $id = $request->get("id");
            $device = Device::findOrFail($id);
            $device->delete();
            return response()->json(['success' => true]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }
    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postAnyData($cust_id = '') {

      $devices = Device:: where('customer_id' ,$cust_id)->get();
        return Datatables::of($devices)
            ->addColumn('action', function ($device) {
                return '<a href="'. action('DeviceController@getEdit', [$device->id]) .'" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o"></i> Edit</a>&nbsp;' .
                '<a  href="'. action('DeviceController@postDelete') . '" data-id="'. $device->id . '" class="btn btn-xs btn-primary btn-delete"><i class="fa fa-trash"></i> Delete</a>&nbsp;';
            })
            ->make(true);
    }
}
