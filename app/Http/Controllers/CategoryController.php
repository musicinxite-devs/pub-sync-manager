<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use Datatables;
use App\Http\Requests;
use Helper;
use Session;

class CategoryController extends Controller
{
    public function getList($type = ''){

        $categories = Category::where('type',$type)->get()->toArray();
        $str ='';
        if($categories) {
            $catArr = Helper::buildTree($categories);
            $str = $this->createHtml($catArr,$type,$str);
        }

        return view('category.index', compact('catArr','str','type'));
    }

    public function getCreate($type = ''){
        $categories = Category::where('type',$type)->get()->toArray();
        $str ='';
        $cat_parent = array();
        if($categories) {
            $catArr = Helper::buildTree($categories);
            $str = Helper::createDropDown($catArr,'',0,'',$cat_parent);
        }
        return view('category.form', compact('type','str'));
    }

    public function getEdit($type = '',$id = '' ){
        $categories = Category::where('type',$type)->get()->toArray();
        $category = Category::findOrFail($id);
        $cat_parent = array();
        $str = '';
        if($categories) {
            $catArr = Helper::buildTree($categories);
            $category_id = $category->parent_id;
            $cat_parent[] = $category_id;
            $str = Helper::createDropDown($catArr,'',0,'',$cat_parent);
        }

        return view('category.form', compact('category','type','str'));
    }

    public function postSave(Request $request) {

        try {
            $id = $request->get("id", '');
            $validator = Category::validator($request->all());
            if ($validator->fails()) {
                // throw errors
                return redirect()->back()
                    ->withErrors($validator->getMessageBag())
                    ->withInput($request->all());
            }

            if ($id == '') {
                $category = new Category();
            } else {
                $category = Category::findOrFail($id);
                if($request->get("parent_id", '') == $id)
                    return redirect()->back()->with('category','Parent and child categories cant\'be same');
            }

                $category->title = $request->get("title", '');
                $category->parent_id = $request->get("parent_id", '');
                $category->status = $request->get("status", '');
                $category->type = $request->get("type", '');
                $category->save();
            //$customer->subscription_start_date = $request->get("subscription_start_date");
            return redirect(action('CategoryController@getList',[$request->get("type", '')]))->with('success');

        } catch (\Exception $ex) {
            $message = $ex->getMessage();
            return redirect()->back()
                ->withErrors($message)
                ->withInput($request->all());
        }
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData() {
        return Datatables::of(Category::all())
            ->addColumn('action', function ($device) {
                return '<a href="'. action('CategoryController@getEdit', [$device->id]) .'" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o"></i> Edit</a>&nbsp;' .
                '<a  href="'. action('CategoryController@postDelete') . '" data-id="'. $device->id . '" class="btn btn-xs btn-primary btn-delete"><i class="fa fa-trash"></i> Delete</a>&nbsp;';
            })
            ->make(true);
    }

    protected function createHtml(array $elements,$type = '',$str = '') {

        //print_r($elements);
         foreach($elements as $key => $cat) {
             $status = ($cat['status'] == "Y")? "Active ":"Inactive ";
             $str .= '<li class="dd-item" data-id='.$cat['id'].'> <div class="handle noDragClass">'.$cat['title'].'
                   <a href="'. action('CategoryController@getEdit', [$type,$cat['id']]) .'" class="pull-right" title="Edit category"><i class="fa fa-pencil-square-o"></i>
                  </a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<label class="pull-right" style="margin-right: 10px"> '.$status.'  </label> </div>';

             if(!empty($cat['children'])) {
                 $str .= '<ol class="dd-list">';
                 $str .= $this->createHtml($cat['children'],$type);
                 $str .="</ol>";
             }

             $str .="</li>";
         }

         return $str;

    }

}
