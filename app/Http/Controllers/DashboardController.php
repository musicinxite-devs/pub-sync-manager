<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\Log;
use App\Models\TrackRemovalRequest;
use Datatables;
use Carbon\Carbon;
use DB;

class DashboardController extends Controller
{
    public function index() {

        //$log = Log::select('event_datetime')->count('event')->where('event_datetime',Carbon::now()->startOfMonth())->get();
     try {

        $logs = DB::table('logs')
            ->select(DB::raw('count(event) as event_count, event,DAY(event_datetime) as event_day'))
            ->whereMonth('event_datetime', '=',Carbon::now()->month)
            ->groupBy('event_datetime')
            ->groupBy('event')
            ->get();

 //dd($logs);
        $events = array();
        $new_events = array();
        $days = date('t');

        foreach ($logs as $log) {
            $events[$log->event][$log->event_day] = $log;
        }
      //  dd(array_keys($events));
        $j = 1;
        foreach ($events as $event => $event_detail) {
           // ${"arr_".$event} = strtolower(str_replace(' ','_',$event));
            $arr_event = array();
            for ($i = 1; $i <= $days; $i++) {
                if (isset($event_detail[$i]))
                $arr_event[] = [$i,$event_detail[$i]->event_count];
               else
                    $arr_event[] = [$i,0];
            }

            $new_events[$event] = $arr_event;
            $j++;
        }
         //$login = $ol = $bl = $open = array();

         $ol = (isset($new_events['Offline']))?$new_events['Offline']:'';
         $login = (isset($new_events['Login']))?$new_events['Login']:'';
         $bl = (isset($new_events['Buffer Low']))?$new_events['Buffer Low']:'';
         $open = (isset($new_events['Open']))?$new_events['Open']:'';

         $customer_name = DB::table('customers')->orderBy('name')->pluck('name','id');
         $streams = DB::table('streams')->orderBy('title')->pluck('title','id');
         $device_model = DB::table('logs')->where('device_model','!=','')->groupBy('device_model')->lists('device_model','device_model');

         $ios_version = DB::table('logs')->where('ios_version','!=','')->groupBy('ios_version')->lists('ios_version','ios_version');

       } catch (\Exception $ex) {


        }

        return view('dashboard.index',compact('ol','open', 'bl', 'login','customer_id','customer_name','device_model','ios_version','streams'));

    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postAnyRequestData(Request $request) {

        $track_request = TrackRemovalRequest::select('track_removal_requests.*');
               //->join('customers', 'logs.customer_id', '=', 'customers.id')
              // ->orderBy('customers.name', 'asc');


        $datatables = Datatables::of($track_request)

            ->addColumn('customer_id', function ($track_request) {
                return $track_request->customer->name;
            })
            ->addColumn('stream_id', function ($track_request) {
                return $track_request->stream->title;
            });

            if($request->has('customer_id')){
                $datatables->where('customer_id', $request->get('customer_id',''));
            }

            if($request->has('stream_id')){
                $datatables->where('stream_id', $request->get('stream_id',''));
            }

            if($request->has('type')){
                $datatables->where('type', $request->get('type',''));
            }
            return $datatables->make(true);
    }

    public function postAnyData(Request $request) {

        $log = Log::select('logs.*','customers.name')
            ->join('customers', function ($join) {
                $join->on('logs.customer_id', '=', 'customers.id');
            });
        //->join('customers', 'logs.customer_id', '=', 'customers.id')
        // ->orderBy('customers.name', 'asc');


        $datatables = Datatables::of($log)

            ->addColumn('customer_id', function ($log) {
                return $log->customer->name;
            });

            if($request->has('customer_name')){
                $datatables->where('customer_id', $request->get('customer_name',''));
            }

            if($request->has('ios_version')){
                $datatables->where('ios_version', $request->get('ios_version',''));
            }

            if ($request->has('start_date') && $request->has('end_date')) {

                $start_date = $request->get('start_date','')." 00:00:00";
                $end_date = $request->get('end_date','')." 23:59:59";

                $datatables->whereBetween('event_datetime', [$start_date,$end_date]);
            }
            if ($request->has('event')) {
                $datatables->where('event', $request->get('event',''));
            }

            if ($request->has('device_model')) {
                $datatables->where('device_model', $request->get('device_model',''));
            }

            if($request->has('unique_data') && $request->has('unique_data') == 'Yes'){
                $datatables->groupBy('customer_id','device_detail');
            }

            return $datatables->make(true);
    }
}