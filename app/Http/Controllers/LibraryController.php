<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Library;
use App\Models\Category;
use App\Models\Stream;
use App\Models\Track;
use Carbon\Carbon;
use \Illuminate\Http\Request;
use Datatables;
use DB;
use Helper;

class LibraryController extends Controller
{
    public function getIndex() {
        $categories = Category::where('type','library')->where('status','Y')->get()->toArray();
        $str = '';
        $cat_parent = array();
        $catArr = array();
        if($categories) {
            $catArr = Helper::buildTree($categories);
            $str = Helper::createDropDown($catArr,'',0,'',$cat_parent);
        }

        return view('library.index',compact('str'));
    }

    public function getCreate() {
        $categories = Category::where('type','library')->where('status','Y')->get()->toArray();
        $tracks = DB::table('tracks')->select(DB::raw("CONCAT(title ,' - ', artist) AS title_artist, id"))->lists('title_artist', 'id');
        $tracks = DB::table('tracks')->select(DB::raw("CONCAT(title ,' - ', artist) AS title_artist, id"))->lists('title_artist', 'id');
        $catArr = array();
        $cat_parent = array();

        if($categories) {
            $catArr = Helper::buildTree($categories);
        }
        $str = Helper::createDropDown($catArr,'',0,'',$cat_parent);
        return view('library.form', compact('tracks','str'));
    }

    public function getEdit($id = '') {
        $categories = Category::where('type','library')->where('status','Y')->get()->toArray();
        $tracks = DB::table('tracks')->select(DB::raw("CONCAT(title ,' - ', artist) AS title_artist, id"))->lists('title_artist', 'id');
        $library = Library::findOrFail($id);

        $cat_parent = array();
        $catArr = array();
        $str = '';
        if($categories) {
            $catArr = Helper::buildTree($categories);
            $category_id = $library->category_id;
            $cat_parent[] = $category_id;
            $str = Helper::createDropDown($catArr,'',0,'',$cat_parent);
        }
//echo '<pre>';
  //      print_r($catArr);
        return view('library.form', compact('library','tracks','str'));
    }

    public function postSave(Request $request) {
        $id = $request->get("id", '');
        $validator = Library::validator($request->all(), $id);
        if($validator->fails()) {
            // throw errors
            return redirect()->back()
                ->withErrors($validator->getMessageBag())
                ->withInput($request->all());
        }
        if($id == '') {
            $library = new Library();
        } else {
            $library = Library::findOrFail($id);
        }
        $library->title = $request->get("title",'');
        $library->status = $request->get("status",'active');
        $library->category_id = $request->get("category_id",null);

        if($library->save()) {
            $tracks = array_filter($request->get("track",[]), function($track) {
                return !($track == '');
            });
            $library->tracks()->sync($tracks);
        } else {
            throw new \Exception(trans('dashboard.unable_to_save_library'));
        }

        $message = trans('dashboard.dynamic.library_'.($id == '' ? 'created' : 'updated').'_success');
        return redirect(action('LibraryController@getIndex'))->with('success', $message);
    }

    public function postDelete(Request $request) {
        try {
            $id = $request->get("id");
            $library = Library::findOrFail($id);

            $exists = Stream::where('library_id', $id)->exists();

            if( $exists ){
                return response()->json(['exist' => true]);
            } else {
                $library->delete();
            }

            return response()->json(['success' => true]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postAnyData(Request $request) {

      //  $library = Library::select('libraries.*');
        $library = Library::select('libraries.*','categories.title AS category_title')
            ->leftjoin('categories', function ($join) {
                $join->on('libraries.category_id', '=', 'categories.id')->where('categories.status','=','Y');
            });

        $datatables = Datatables::of($library)

            ->addColumn('action', function ($library) {
                return '<a href="'. action('LibraryController@getEdit', [$library->id]) .'" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o"></i> Edit</a>&nbsp;' .
                '<a  href="'. action('LibraryController@postDelete') . '" data-id="'. $library->id . '" class="btn btn-xs btn-primary btn-delete"><i class="fa fa-trash"></i> Delete</a>';
            })
            ->addColumn('category_title', function ($library) {
                return $library->category_title;
            });
             if($request->has('category_id')){
                 $datatables->where('category_id', $request->get('category_id',''));
             }
           return $datatables->make(true);
    }
}
