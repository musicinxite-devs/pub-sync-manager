<?php

namespace App\Http\Controllers;

use \Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Role;
use Datatables;
use App\Models\User;
use Auth;


class AccountController extends Controller
{
    public function getIndex() {
        $user = Auth::user();
        return view('account.index', compact('user'));
    }

    public function postSave(Request $request) {
        $validator = User::validator($request->all(), $request->get("id", ''));
        if($validator->fails()) {
            // throw errors
            return redirect()->back()
                ->withErrors($validator->getMessageBag())
                ->withInput($request->all());
        }
        $user = User::findOrFail($request->get("id"));
        $user->name = $request->get("name",'');
        $user->email = $request->get("email",'');
        $user->role_id = $request->get("role_id",'');
        if($request->get("new_password",'') != '') {
            $user->password = bcrypt($request->get("new_password"));
        }
        $user->save();
        return redirect(action('AccountController@getIndex'))->with('success', trans('dashboard.account_settings_changed_success'));
    }
}
