<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Track;
use App\Models\Category;
use Carbon\Carbon;
use \Illuminate\Http\Request;
use Datatables;
use Helper;
use Storage;
use Symfony\Component\HttpFoundation\File\File;

class TrackController extends Controller
{
    public function getIndex() {

        $categories = Category::where('type','track')->where('status','Y')->get()->toArray();
        $str = '';
        $cat_parent = array();
        $catArr = array();
        if($categories) {
            $catArr = Helper::buildTree($categories);
            $str = Helper::createDropDown($catArr,'',0,'',$cat_parent);
        }

        return view('track.index',compact('str'));
    }

    public function getCreate() {
        return view('track.create');
    }

    public function getEdit($id = '') {

        $categories = Category::where('type','track')->where('status','Y')->get()->toArray();
        $catArr = array();
        $cat_parent = array();
        $track = Track::findOrFail($id);
        if($categories) {
            $catArr = Helper::buildTree($categories);
            $categories = $track->categories->toArray();

            $cat_parent = array();
            foreach($categories as $cat){
                $cat_parent[] = $cat['id'];
            }
        }

        $str = Helper::createDropDown($catArr,'',0,'',$cat_parent);
        return view('track.form', compact('track','str'));
    }

    public function postSave(Request $request) {

        $filePaths = $request->get('file_path', []);
        $title = $request->get('title', []);
        $artist = $request->get('artist', []);
        $album = $request->get('album', []);

        foreach($filePaths as $key => $path) {
            $track = new Track();

            $track->title = $title[$key];
            $track->artist = $artist[$key];
            $track->album = $album[$key];
            $track->file_path = $path;

            $track->save();
        }

        $message = trans('dashboard.dynamic.track_created_success');

        return redirect(action('TrackController@getIndex'))->with('success', $message);
    }

    public function postUpdate(Request $request) {
        $id = $request->get("id", '');
        $validator = Track::validator($request->all(), $id);
        if($validator->fails()) {
            // throw errors
            return redirect()->back()
                ->withErrors($validator->getMessageBag())
                ->withInput($request->all());
        }
        $track = Track::findOrFail($id);
            $track->title = $request->get("title",'');
        $track->artist = $request->get("artist",'');
        $track->file_path = $request->get("file_path",'');
        $track->save();

        $processes = ($request->get('category_id') ?: []);

        $track->categories()->sync($processes);
        $message = trans('dashboard.dynamic.track_updated_success');
        return redirect(action('TrackController@getIndex'))->with('success', $message);
    }

    public function postUpload()
    {

        try {
            $trackFiles = request()->file('track-file');

            $destination = config('music-inxite.common.storage_temp_dir');
//            if(!\File::exists($destination)) {
//                \File::makeDirectory($destination,755,true);
//            }
            if(is_array($trackFiles)) {
                $trackFiles = $trackFiles[0];
            }
            $file = $trackFiles->getRealPath();

            $filename = $trackFiles->getClientOriginalName();

            if(!\File::exists(realpath(storage_path($destination))."/".$filename)) {

                $trackFiles->move(realpath(storage_path($destination)), $filename);
            }

            $title="";
            $artist="";
            $album="";

            if ($fp_remote = fopen(realpath(storage_path($destination))."/".$filename, 'r')) {
//print_r($fp_remote);

                $localtempfilename = tempnam('/tmp', 'getID3');
                if ($fp_local = fopen($localtempfilename, 'wb')) {
                    while ($buffer = fread($fp_remote, 8192)) {
                        fwrite($fp_local, $buffer);
                    }
                    fclose($fp_local);
                    // Initialize getID3 engine
                    $getID3 = new \getID3;
                    $tagInfo = $getID3->analyze($localtempfilename);
                  //  print_r($tagInfo);
                    if (isset($tagInfo['tags'])) {
                        $title = isset($tagInfo['tags']['id3v1']['title'][0]) ? trim($tagInfo['tags']['id3v1']['title'][0]) : "";
                        $artist = isset($tagInfo['tags']['id3v1']['artist'][0]) ? trim($tagInfo['tags']['id3v1']['artist'][0]) : "";
                        $album = isset($tagInfo['tags']['id3v1']['album'][0]) ? trim($tagInfo['tags']['id3v1']['album'][0]) : $artist;
                    }
                    // Delete temporary file
                    unlink($localtempfilename);
                }
                fclose($fp_remote);
            }

            $exists = Track::where('title', $title)->exists();
            ($exists) ? $track_exist = trans('dashboard.title_exist'):$track_exist = '';

            return [
                "status" => 200,
                "file_path" => $filename,
                "title" => $title,
                "artist" => $artist,
                "album" => $album,
                "track_exist" => $track_exist,
                ];

            } catch (\Exception $ex) {

            return [
                'status' => 500,
                'message' => "Error: uploading file"
            ];
        }
    }

    public function postDelete(Request $request) {
        try {
            $id = $request->get("id");
            $track = Track::findOrFail($id);
            $exists = $track->libraries()->where('track_id', $id)->exists();

            if( $exists ){
                return response()->json(['exist' => true]);
            } else {
                $track->delete();
            }


            return response()->json(['success' => true]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postAnyData(Request $request) {
        $track = Track::select('tracks.*','category_track.category_id','categories.title AS category_title')
                ->leftjoin('category_track', function ($join) {
                    $join->on('tracks.id', '=', 'category_track.track_id');
                })
                ->leftjoin('categories', function ($join) {
                    $join->on('category_track.category_id', '=', 'categories.id')->where('categories.status','=','Y');
                });

        $datatables = Datatables::of($track)

            ->addColumn('category_title', function ($track) {
                return $track->category_title;
            })
            ->addColumn('action', function ($track) {
                return '<a href="'. action('TrackController@getEdit', [$track->id]).'" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o"></i> Edit</a>&nbsp;' .
                '<a  href="'. action('TrackController@postDelete'). '" data-id="'. $track->id . '" class="btn btn-xs btn-primary btn-delete"><i class="fa fa-trash"></i> Delete</a>';
            });

            if($request->has('category_id')){
                $datatables->where('category_id', $request->get('category_id',''));
            }

            return $datatables->make(true);
    }

    public function getDownload($id) {
        try {
            $track = Track::findOrFail($id);
            $trackFilePath = storage_path(). DIRECTORY_SEPARATOR . config('music-inxite.common.storage_temp_dir') . DIRECTORY_SEPARATOR . $track->file_path;
            return response()->download($trackFilePath, $track->file_path);
        } catch (\Exception $ex) {
            echo $ex->getMessage();
        }
    }

    public function postDuplicateTrack(Request $request) {

        try {

            $filePaths = $request->get('file_path', []);
            $title = $request->get('title', []);
            $error = array();
            $status = 200;

            foreach($filePaths as $key => $path) {
                $track = new Track();

                $track->title = $title[$key];
                $track->file_path = $path;
                $exists = Track::where('title', $title[$key])->exists();
                ($exists) ? $error[] = trans('dashboard.title_exist') AND $status = '' :'';
            }

            return response()->json(['success' => true,"status" => $status,"error" => $error]);

        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }
}
