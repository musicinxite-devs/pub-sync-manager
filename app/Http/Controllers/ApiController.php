<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Device;
use App\Models\Log;
use App\Models\Stream;
use App\Models\TrackRemovalRequest;
use Carbon\Carbon;
use \Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;

class ApiController extends Controller
{
    public function postAuthenticate(Request $request) {
        $email = $request->get('email', '');
        $password = $request->get('password', '');
        $uu_id = $request->get('uu_id', '');

        $customer = Customer::where('email', $email)->where('password', $password)->get();

        if($customer->count() > 0) {
            $customer = $customer->first();
            $device = Device::where('uu_id', $uu_id)->where('customer_id', $customer->id)->get();
            if($device->count() > 0 ) {
                $device_active = Device::where('login_status', 'active')->where('customer_id', $customer->id)->get();
                /*
                 *  To check how many devices actively logged in
                *  if exceeded  max_concurrent_login then do not allow the user
                */
                if($device_active->count() < $customer->max_concurrent_login ) {
                /*
                 * Update login_status for the device
                 */
                    $device_uu_id = Device::where('uu_id', $uu_id)->first();
                    $device_uu_id->login_status = 'active';
                    $device_uu_id->save();

                    $response = [
                        'status' => 200,
                        'customer' => [
                            'customer_id' => $customer->id,
                            'name' => $customer->name,
                            'email' => $customer->email,
                            'phone' => $customer->phone,
                            'pin' => ($customer->pin)? $customer->pin: 0,
                            'subscription_start_date'  => $customer->subscription_start_date->toDateTimeString(),
                            'subscription_end_date' => $customer->subscription_end_date->toDateTimeString()
                        ],
                        'streams' => []
                    ];

                    foreach($customer->streams as $stream) {
                        $tracks = [];
                        $libraryTracks = $stream->library->tracks()->get(['tracks.id', 'title', 'updated_at']);
                        foreach($libraryTracks as $libraryTrack) {
                            $tracks[] = [
                                'id' => $libraryTrack->id,
                                'title' => $libraryTrack->title,
                                'updated_at' =>$libraryTrack->updated_at->toDateTimeString()
                            ];
                        }
                        $response['streams'][] = [
                            'id' => $stream->id,
                            'title' => $stream->title,
                            'url' => $stream->url,
                            'status' => $stream->status,
                            'updated_at' => $stream->updated_at->toDateTimeString(),
                            'offline_library' => [
                                'id' => $stream->library->id,
                                'title' => $stream->library->title,
                                'status' => $stream->library->status,
                                'updated_at' => $stream->library->updated_at->toDateTimeString(),
                                'tracks' => $tracks
                            ]
                        ];
                    }

                    return $response;
                } else {
                    $message = "Maximum limit reached";
                }
            } else {
                $message = "Your device is not registered. Please contact administrator.";
            }
        } else {
            $message = "Invalid login credentials!";
        }

        return [
            'status' => 400,
            'message' => $message
        ];
    }

    public function postLog(Request $request) {
        try {
            $event = $request->get('event', '');
            $event_datetime = $request->get('event_datetime', '');
            $device_detail = $request->get('device_detail', '');
            $customer_id = $request->get('customer_id', '');
            $model = $request->get('model', '');
            $ios_version= $request->get('OSVersion', '');

            Log::create([
                'customer_id' => $customer_id,
                'event' => $event,
                'event_datetime' => Carbon::createFromFormat(Carbon::DEFAULT_TO_STRING_FORMAT, $event_datetime),
                'device_detail' => $device_detail,
                'device_model' => $model,
                'ios_version' => $ios_version
            ]);
            return [
                'status' => 200,
                'message' => 'Log created successfully!'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 400,
                'message' => $ex->getMessage()
            ];
        }
    }


    public function postForgetPassword(Request $request) {
        $email = $request->get('email', '');
        $customer = Customer::where('email', $email)->first();
        if($customer) {
            $password = Str::random(10);
            $customer->password = $password;
            $customer->save();
            $data = [
                'password' => $password
            ];
            $mailFrom = config('mail.from');
            Mail::queue('emails.password_reset', $data,
                function($message) use ($email, $mailFrom) {
                    $message
                        ->from($mailFrom['address'], $mailFrom['name'])
                        ->to($email)
                        ->subject("Music Inxite: Password Reset");
                });
            return [
                'status' => 200,
                'message' => 'Password reset success!'
            ];
        } else {
            return [
                'status' => 400,
                'message' => 'Email does not exists!'
            ];
        }
    }

    public function postTerminate(Request $request) {
        try {
            $uu_id = $request->get('uu_id', '');

            $device_uu_id = Device::where('uu_id', $uu_id)->first();
            $device_uu_id->login_status = 'inactive';
            $device_uu_id->save();

            return [
                'status' => 200,
                'message' => 'Log out successfully!'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 400,
                'message' => $ex->getMessage()
            ];
        }

    }

    public function postRemoval(Request $request) {
        try {
            $stream_id = $request->get('stream_id', '');
            $customer_id = $request->get('customer_id', '');
            $meta_data = $request->get('meta_data', '');
            $type = $request->get('type', '');

            $track_removal = TrackRemovalRequest::create([
                'customer_id' => $customer_id,
                'stream_id' => $stream_id,
                'meta_data' => $meta_data,
                'type' => $type,
            ]);

            $customer = Customer::where('id', $customer_id)->first();
            $stream = Stream::where('id', $stream_id)->first();

            if($type == 'removal')
                 $title = "You have received a song removal  like request";
            else
                 $title = "You have received a song more like song request";

            $date = date('m/d/Y H:i:s');
                Mail::send('emails.removal_request', ['customer' => $customer,'stream' => $stream, 'title' =>$title,'date' =>$date,'track_removal' => $track_removal],
                    function ($m) use ($customer) {
                    $m->to(($customer->removal_request_email)? $customer->removal_request_email : "patrick@musicinxite.com" , $customer->name)->subject('Track removal/more like request');
                });


            return [
                'status' => 200,
                'message' => 'Request created successfully!'
            ];
        } catch (\Exception $ex) {
            return [
                'status' => 400,
                'message' => $ex->getMessage()
            ];
        }
    }




}