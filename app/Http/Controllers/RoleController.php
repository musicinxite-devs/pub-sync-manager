<?php

namespace App\Http\Controllers;

use App\Models\Permission;
use \Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Role;
use Datatables;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    public function getIndex() {
        return view('role.index');
    }

    public function getCreate() {
        $permissions = Auth::user()->role->permissions()->get();
        return view('role.form', compact('permissions'));
    }

    public function getEdit($id) {
        $permissions = Permission::get();
        $role = Role::with('permissions')->findOrFail($id);
        $rolePermissions = $role->permissions()->lists("permission_id")->toArray();
        return view('role.form', compact('role', 'permissions', 'rolePermissions'));
    }

    public function postSave(Request $request) {
        $id = $request->get("id", '');
        $validator = Role::validator($request->all(), $id);
        if($validator->fails()) {
            // throw errors
            return redirect()->back()
                ->withErrors($validator->getMessageBag())
                ->withInput($request->all());
        }
        if($id == '') {
            $role = new Role();
        } else {
            $role = Role::findOrFail($id);
        }
        $role->name = $request->get("name",'');
        $role->save();

        // Set Role Permissions
        $permissions = $request->get('permission', []);
        $role->permissions()->sync($permissions);

        $message = trans('dashboard.dynamic.role_'.($id == '' ? 'created' : 'updated').'_success');
        return redirect(action('RoleController@getIndex'))->with('success', $message);
    }

    public function postDelete(Request $request) {
        try {
            $id = $request->get("id");
            $role = Role::findOrFail($id);
            $role->delete();
            return response()->json(['success' => true]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function anyData()
    {
        return Datatables::of(Role::all())
            ->addColumn('action', function ($role) {
                return ($role->is_system_role) ? '' : '<a href="'. action('RoleController@getEdit', [$role->id]) .
                            '"class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o"></i> '.
                            trans('dashboard.edit').'</a>&nbsp;' .
                            '<a href="'. action('RoleController@postDelete') . '" data-id="'. $role->id . '" class="btn btn-xs btn-primary btn-delete">'.
                            '<i class="fa fa-trash"></i> '.trans('dashboard.delete').'</a>';
            })
            ->make(true);
    }
}
