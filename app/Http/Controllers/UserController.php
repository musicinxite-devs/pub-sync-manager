<?php

namespace App\Http\Controllers;

use \Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Role;
use Datatables;
use App\Models\User;

class UserController extends Controller
{
    public function getIndex() {
        return view('user.index');
    }

    
    public function getCreate($id = '') {
        $roles = Role::all();
        
        return view('user.form', compact(
            'roles'
        ));
    }

    public function getEdit($id = '') {
        $roles = Role::all();
        $user = User::findOrFail($id);

		return view('user.form', compact(
            'user',
			'roles'
        ));
    }

    public function postSave(Request $request) {
        $id = $request->get("id", '');
        $validator = User::validator($request->all(), $id);
        if($validator->fails()) {
            // throw errors
            return redirect()->back()
                ->withErrors($validator->getMessageBag())
                ->withInput($request->all());
        }
        if($id == '') {
            $user = new User();
        } else {
            $user = User::findOrFail($id);
        }
        $user->name = $request->get("name",'');
        $user->email = $request->get("email",'');
        $user->is_system_user = false;
		$user->is_active = $request->get("is_active",'0');
        $user->role_id = $request->get("role_id",'');
        if($request->get("password",'') != '') {
            $user->password = bcrypt($request->get("password"));
        }
        $user->save();        

        $message = trans('dashboard.dynamic.user_'.($id == '' ? 'created' : 'updated').'_success');
        return redirect(action('UserController@getIndex'))->with('success', $message);
    }

    public function postDelete(Request $request) {
        try {
            $id = $request->get("id");
            $user = User::findOrFail($id);
            $user->delete();
            return response()->json(['success' => true]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postAnyData() {
        return Datatables::of(User::select('users.*', 'roles.name')->join('roles', 'users.role_id', '=', 'roles.id'))
            ->addColumn('action', function ($user) {
                if($user->is_system_user) return '';

                return '<a href="'. action('UserController@getEdit', [$user->id]) .'" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o"></i> Edit</a>&nbsp;' .
                    '<a  href="'. action('UserController@postDelete') . '" data-id="'. $user->id . '" class="btn btn-xs btn-primary btn-delete"><i class="fa fa-trash"></i> Delete</a>';
            })
            ->make(true);
    }
}
