<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Category;
use App\Models\Stream;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Datatables;
use Helper;
use Illuminate\Support\MessageBag;

class CustomerController extends Controller
{
    public function getIndex() {
        $categories = Category::where('type','category')->where('status','Y')->get()->toArray();
        $str = '';
        $cat_parent = array();
        $catArr = array();
        if($categories) {
            $catArr = Helper::buildTree($categories);
            $str = Helper::createDropDown($catArr,'',0,'',$cat_parent);
        }
        return view('customer.index',compact('str'));
    }

    public function getCreate() {
        $categories = Category::where('type','category')->where('status','Y')->get()->toArray();
        $streams = Stream::all();
        $date = date("m/d/Y");
        $cat_parent = array();
        $catArr = array();

        if($categories) {
            $catArr = Helper::buildTree($categories);
        }
        $str = Helper::createDropDown($catArr,'',0,'',$cat_parent);
        return view('customer.form', compact('streams','date','str'));
    }

    public function getEdit($id = '') {
        $categories = Category::where('type','category')->where('status','Y')->get()->toArray();
        $streams = Stream::all();
        $customer = Customer::findOrFail($id);
        //echo '<pre>';
//print_R($customer->streams);die;
        $date = date("Y-m-d");
        $cat_parent = array();
        $catArr = array();
        $str = '';

        if($categories) {
            $catArr = Helper::buildTree($categories);
            $category_id = $customer->category_id;
            $cat_parent[] = $category_id;
            $str = Helper::createDropDown($catArr,'',0,'',$cat_parent);
        }
        $stream_arr = $streams->pluck('title', 'id');
        return view('customer.form', compact(
            'customer',
            'stream_arr',
            'streams',
            'date',
            'str'
        ));
    }

    public function postSave(Request $request) {
        /*echo '<pre>';
        echo $request->get("subscription_start_date");
        print_r($_POST); exit;*/
        $id = $request->get("id", '');
        $validator = Customer::validator($request->all(), $id);


        if($validator->fails()) {
            // throw errors
            $stream = $request->input('stream');
           // print_R($stream);die;
            return redirect()->back()
                ->withErrors($validator->getMessageBag())
                ->withInput($request->all());
        }

        if($id == '') {
            $customer = new Customer();
        } else {
            $customer = Customer::findOrFail($id);
        }

        $customer->name = $request->get("name",'');
        $customer->email = $request->get("email",'');
        $customer->phone = $request->get("phone");
        $customer->category_id = $request->get("category_id",null);
        $customer->max_concurrent_login = $request->get("max_concurrent_login");
        $customer->pin = $request->get("pin",null);
        $customer->removal_request_email = $request->get("removal_request_email");
        //$customer->subscription_start_date = $request->get("subscription_start_date");


        $subscriptionEndDate = $subscriptionStartDate = null;
        if($request->get("subscription_start_date", '') != '') {
            $subscriptionStartDate = Carbon::createFromFormat(config('music-inxite.common.dateformat.php'), $request->get("subscription_start_date", ''));
        }

        if($request->get("subscription_end_date", '') != '') {
            //Carbon::createFromFormat('d-m-Y',$request->get('start_date',''))->format('Y-m-d');
            $subscriptionEndDate = Carbon::createFromFormat(config('music-inxite.common.dateformat.php'), $request->get("subscription_end_date", ''));
        }

        $customer->subscription_start_date = $subscriptionStartDate;
        $customer->subscription_end_date = $subscriptionEndDate;

        if($request->get("password",'') != '') {
            $customer->password = $request->get("password");
        }

        if($customer->save()) {
            $streams = array_filter($request->get("stream",[]), function($stream) {
                return !($stream == '');
            });
            $customer->streams()->sync($streams);
        } else {
            throw new \Exception(trans('dashboard.unable_to_save_customer'));
        }

        $message = trans('dashboard.dynamic.customer_'.($id == '' ? 'created' : 'updated').'_success');
        return redirect(action('CustomerController@getIndex'))->with('success', $message);
    }

    public function postDelete(Request $request) {
        try {
            $id = $request->get("id");
            $customer = Customer::findOrFail($id);
            $customer->delete();
            return response()->json(['success' => true]);
        } catch (\Exception $ex) {
            return response()->json(['error' => $ex->getMessage()]);
        }
    }

    /**
     * Process datatables ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function postAnyData(Request $request) {

        $customer = Customer::select('customers.*','categories.title AS category_title')
            ->leftjoin('categories', function ($join) {
                $join->on('customers.category_id', '=', 'categories.id')->where('categories.status','=','Y');
            });

        $datatables = Datatables::of($customer)
            ->addColumn('category_title', function ($customer) {
                return $customer->category_title;
            })
            ->addColumn('subscription_start_date', function ($customer) {
               return $customer->getSubscriptionStartDate(config('music-inxite.common.dateformat.php'));
            })
            ->addColumn('subscription_end_date', function ($customer) {
                return $customer->getSubscriptionEndDate(config('music-inxite.common.dateformat.php'));
            })
            ->addColumn('action', function ($customer) {
                return '<a href="'. action('CustomerController@getEdit', [$customer->id]) .'" class="btn btn-xs btn-primary"><i class="fa fa-pencil-square-o"></i> Edit</a>&nbsp;' .
                '<a  href="'. action('CustomerController@postDelete') . '" data-id="'. $customer->id . '" class="btn btn-xs btn-primary btn-delete"><i class="fa fa-trash"></i> Delete</a>&nbsp;'.
                '<a  href="'. action('DeviceController@getList', [$customer->id]) .'" class="btn btn-xs btn-primary"><i class="fa fa-mobile"></i> Devices</a>';
            })
           ;
            if($request->has('category_id')){
                $datatables->where('category_id', $request->get('category_id',''));
            }
            return $datatables->make(true);
    }
}
