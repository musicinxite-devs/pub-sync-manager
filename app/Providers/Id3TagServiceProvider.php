<?php
/**
 * Created by PhpStorm.
 * User: AVDEVS_Workstation
 * Date: 25/10/16
 * Time: 5:07 PM
 */

namespace App\Providers;

use App\Services\Id3TagService;
use Illuminate\Support\ServiceProvider;

class Id3TagServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(Id3TagService::class, function ($app) {
            return new Id3TagService();
        });
    }
}