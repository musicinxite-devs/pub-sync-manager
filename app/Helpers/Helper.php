<?php
/**
 * Created by PhpStorm.
 * User: Apple
 * Date: 30-01-2017
 * Time: 01:24 PM
 */
namespace App\Helpers;

class Helper
{
    public static function buildTree(array $catArr, $parentId = 0) {

        foreach($catArr as $key => $cat){

            $elements[$cat['id']] = $cat;
        }

        $catArr = array();

        $branch = array();

        foreach ($elements as $element) {
            if ($element['parent_id'] == $parentId) {
                $children = self::buildTree($elements, $element['id']);
                if ($children) {
                    $element['children'] = $children;
                }
                $branch[] = $element;
            }
        }

        return $branch;
    }

    public static function createDropDown(array $elements,$r = 0,$parent_id = 0, $str = '',array $cat_parent) {

        foreach($elements as $key => $cat) {
            $selected = '';
            $dash = ($cat['parent_id'] == 0) ? '' : str_repeat('-', $r) .' ';

            if ($cat['parent_id'] == $parent_id) {
                // reset $r
                $r = 0;
            }
            (in_array($cat["id"],$cat_parent))? $selected = " selected":" ";

            $str .= '<option value='.$cat["id"].$selected.' >'.$dash.$cat["title"].'  </option>';

            if(!empty($cat['children'])) {
                $str .= self::createDropDown($cat['children'],++$r,$cat['parent_id'],'',$cat_parent);
            }
        }
        // echo $str;
        return $str;

    }

    public static function createBreadCrumDropDown(array $elements,$cat_title = '',$parent_id = 0, $str = '',array $cat_parent) {


        $print = false;
        $root_parent = '';
        foreach($elements as $key => $cat) {


            $selected = '';
            (in_array($cat["id"],$cat_parent))? $selected = " selected":" ";

            if($cat['parent_id'] == 0) {
                $root_parent = $cat["title"];
                $cat_title = '';
            } else {

            }
            $cat_title .=  $cat["title"];

            if(!empty($cat['children'])) {
                $cat_title .= ' >> ';
                $str .= self::createBreadCrumDropDown($cat['children'],$cat_title,$cat['parent_id'],'',$cat_parent);
            } else {
                $str .= '<option value='.$cat["id"].$selected.' >'.$cat_title.'  </option>';

                $cat_title = $root_parent ;
            }

        }
        // echo $str;
        return $str;

    }
}