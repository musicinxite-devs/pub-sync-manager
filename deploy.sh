#!/bin/sh

[ "x${SOURCE_PATH}" = "x" ] && SOURCE_PATH="${PWD}/"
[ "x${TARGET_USER}" = "x" ] && TARGET_USER="avdevs"
[ "x${TARGET_HOST}" = "x" ] && TARGET_HOST="musicinxite.com"
[ "x${TARGET_PATH}" = "x" ] && TARGET_PATH="/var/www/playsync-test.musicinxite.com/"

if [ -d "${SOURCE_PATH}" ]; then
    >&2 echo "Syncing files..."
    rsync -rlx --exclude=".gitignore" --exclude="/.git" --exclude="/deploy.sh" --exclude="/composer.lock" --exclude="/music_inxite.rar" "${SOURCE_PATH}/" "${TARGET_USER}@${TARGET_HOST}:${TARGET_PATH}/"
    >&2 echo "Recalculating ACL masks..."
    ssh "${TARGET_USER}@${TARGET_HOST}" sudo /usr/bin/setfacl -R --mask -m "m::---" "${TARGET_PATH}"
else
    >&2 echo "ERROR: ${SOURCE_PATH} does not exist."
fi
