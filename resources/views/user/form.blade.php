@extends('layouts.default')

@section('header')
<div class="row">
    <div class="col-lg-12">
        <h2>{{ trans('dashboard.user_management') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ action('DashboardController@index') }}"><span class="nav-label">{{ trans('dashboard.dashboard') }}</span></a>
            </li>
            <li>
                <a href="{{ action('UserController@getIndex') }}"><span class="nav-label">{{ trans('dashboard.user_management') }}</span></a>
            </li>
            <li class="active">
                <strong>{{ isset($user) ? trans('dashboard.update_user') : trans('dashboard.add_user') }}</strong>
            </li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ action('UserController@postSave') }}">
                    {!! csrf_field() !!}
                    {{ Form::hidden('id', isset($user) ? $user->id : '') }}
                    {{ Form::hidden('customer_id', isset($customer) ? $customer->id : '') }}
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {{Form::label('name', trans('dashboard.name'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('name',old('name', isset($user) ? $user->name : ''),['class' => 'form-control'])}}

                            @if ($errors->has('name'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('name') }}</strong>
                                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {{Form::label('email', trans('dashboard.email'),['class' => 'col-md-4 control-label'])}}

                        <div class="col-md-6">
                            {{Form::email('email',old('email', isset($user) ? $user->email : ''),['class' => 'form-control'])}}

                            @if ($errors->has('email'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }}">
                        {{Form::label('role_id', trans('dashboard.role'),['class' => 'col-md-4 control-label'])}}

                        <div class="col-md-6">
                            {{Form::select('role_id',['' => trans('dashboard.select_role')] + $roles->lists('name', 'id')->toArray(), old('role', isset($user) ? $user->role_id : ''), ['class' => 'form-control select'])}}

                            @if ($errors->has('role_id'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('role_id') }}</strong>
                                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        {{Form::label('password', trans('dashboard.password'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::password('password', ['class' => 'form-control'])}}

                            @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        {{Form::label('password', trans('dashboard.confirm_password'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::password('password_confirmation', ['class' => 'form-control'])}}

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        {{ Form::label('status', trans('dashboard.active'), ['class' => 'col-lg-4 control-label']) }}
                        <div class="col-md-6">
                            {{ Form::checkbox('is_active', '1', isset($user) && $user->is_active == 1) }}
                        </div>
                    </div>

                    <br />
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn">
                                <i class="fa fa-btn fa-check-square-o"></i> {{ trans('dashboard.save') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
<script>

</script>
@endsection