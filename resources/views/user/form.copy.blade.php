@extends('layouts.admin.default')

@section('header')
<div class="row">
    <div class="col-lg-12">
        <h2>{{ trans('dashboard.user_management') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ action('DashboardController@index') }}"><span class="nav-label">{{ trans('dashboard.dashboard') }}</span></a>
            </li>
            <li>
                <a href="{{ action('UserController@getIndex') }}"><span class="nav-label">{{ trans('dashboard.user_management') }}</span></a>
            </li>
            <li class="active">
                <strong>{{ isset($user) ? trans('dashboard.update_user') : trans('dashboard.add_user') }}</strong>
            </li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ action('Admin\UserController@postSave') }}">
                    {!! csrf_field() !!}
                    {{ Form::hidden('id', isset($user) ? $user->id : '') }}
                    {{ Form::hidden('customer_id', isset($customer) ? $customer->id : '') }}

                    <div class="form-group{{ $errors->has('salutation') ? ' has-error' : '' }}">
                        {{Form::label('salutation', trans('dashboard.salutation'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('salutation',old('salutation', isset($user) ? $user->salutation : ''),['class' => 'form-control'])}}

                            @if ($errors->has('salutation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('salutation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                        {{Form::label('first_name', trans('dashboard.first_name'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('first_name',old('first_name', isset($user) ? $user->first_name : ''),['class' => 'form-control'])}}

                            @if ($errors->has('first_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('first_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                        {{Form::label('last_name', trans('dashboard.last_name'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('last_name',old('last_name', isset($user) ? $user->last_name : ''),['class' => 'form-control'])}}

                            @if ($errors->has('last_name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('last_name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {{Form::label('email', trans('dashboard.email'),['class' => 'col-md-4 control-label'])}}

                        <div class="col-md-6">
                            {{Form::email('email',old('email', isset($user) ? $user->email : ''),['class' => 'form-control'])}}

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('mobile_phone') ? ' has-error' : '' }}">
                        {{Form::label('mobile_phone', trans('dashboard.mobile_phone'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('mobile_phone',old('mobile_phone', isset($user) ? $user->mobile_phone : ''),['class' => 'form-control'])}}

                            @if ($errors->has('mobile_phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('mobile_phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('office_phone') ? ' has-error' : '' }}">
                        {{Form::label('office_phone', trans('dashboard.office_phone'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('office_phone',old('office_phone', isset($user) ? $user->office_phone : ''),['class' => 'form-control'])}}

                            @if ($errors->has('office_phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('office_phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group{{ $errors->has('role_id') ? ' has-error' : '' }}">
                        {{Form::label('role_id', trans('dashboard.role'),['class' => 'col-md-4 control-label'])}}

                        <div class="col-md-6">
                            {{Form::select('role_id',['' => trans('dashboard.select_role')] + $roles->lists('name', 'id')->toArray(), old('role', isset($user) ? $user->role_id : ''), ['class' => 'form-control select'])}}

                            @if ($errors->has('role_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('role_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        {{Form::label('password', trans('dashboard.password'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::password('password', ['class' => 'form-control'])}}

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        {{Form::label('password', trans('dashboard.confirm_password'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::password('password_confirmation', ['class' => 'form-control'])}}

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    @if(isset($contacts))
                        <div class="form-group{{ $errors->has('account_id') ? ' has-error' : '' }}">
                            {{Form::label('account_id', trans('dashboard.account_id'),['class' => 'col-md-4 control-label'])}}

                            <div class="col-md-6">
                                {{Form::select('account_id',['' => trans('dashboard.account_id')] + $contacts, old('account_id', ''), ['class' => 'form-control select'])}}

                                @if ($errors->has('account_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('account_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    @elseif(isset($customer))
                        <div class="form-group{{ $errors->has('account_id') ? ' has-error' : '' }}">
                            {{Form::label('account_id', trans('dashboard.account_id'),['class' => 'col-md-4 control-label'])}}
                            <div class="col-md-6">
                                {{Form::text('account_id',old('account_id', $customer->account_id),['class' => 'form-control'])}}

                                @if ($errors->has('account_id'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('account_id') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                    @endif

                    <div class="form-group{{ $errors->has('estimate_template_id') ? ' has-error' : '' }}">
                        {{Form::label('estimate_template_id', trans('dashboard.estimate_template'),['class' => 'col-md-4 control-label'])}}

                        <div class="col-md-6">
                            {{Form::select('estimate_template_id',['' => trans('dashboard.estimate_template')] + $estimate_templates, old('estimate_template_id', $customer_estimate_template_id), ['class' => 'form-control select'])}}

                            @if ($errors->has('estimate_template_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('estimate_template_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('invoice_template_id') ? ' has-error' : '' }}">
                        {{Form::label('invoice_template_id', trans('dashboard.invoice_template'),['class' => 'col-md-4 control-label'])}}

                        <div class="col-md-6">
                            {{Form::select('invoice_template_id',['' => trans('dashboard.invoice_template')] + $invoice_templates, old('invoice_template_id', $customer_invoice_template_id), ['class' => 'form-control select'])}}

                            @if ($errors->has('invoice_template_id'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('invoice_template_id') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('sales_person') ? ' has-error' : '' }}">
                        {{Form::label('sales_person', trans('dashboard.sales_person'),['class' => 'col-md-4 control-label'])}}

                        <div class="col-md-6">
                            {{Form::text('sales_person',old('sales_person', isset($customer) ? $customer->sales_person : ''),['class' => 'form-control'])}}

                            @if ($errors->has('sales_person'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('sales_person') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group{{ $errors->has('sales_manager') ? ' has-error' : '' }}">
                        {{Form::label('sales_manager', trans('dashboard.sales_manager'),['class' => 'col-md-4 control-label'])}}

                        <div class="col-md-6">
                            {{Form::select('sales_manager',[0 => trans('dashboard.sales_manager')] + $clientServicingStaff, old('sales_manager', isset($customer) ? $customer->sales_manager : 0), ['class' => 'form-control select'])}}

                            @if ($errors->has('sales_manager'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('sales_manager') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('account_manager') ? ' has-error' : '' }}">
                        {{Form::label('account_manager', trans('dashboard.account_manager'),['class' => 'col-md-4 control-label'])}}

                        <div class="col-md-6">
                            {{Form::select('account_manager',[0 => trans('dashboard.account_manager')] + $clientServicingStaff, old('account_manager', isset($customer) ? $customer->account_manager : 0), ['class' => 'form-control select'])}}

                            @if ($errors->has('account_manager'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('account_manager') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('support_manager') ? ' has-error' : '' }}">
                        {{Form::label('support_manager', trans('dashboard.support_manager'),['class' => 'col-md-4 control-label'])}}

                        <div class="col-md-6">
                            {{Form::select('support_manager',[0 => trans('dashboard.support_manager')] + $clientServicingStaff, old('support_manager', isset($customer) ? $customer->support_manager : 0), ['class' => 'form-control select'])}}

                            @if ($errors->has('support_manager'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('support_manager') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="hr-line-dashed"></div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn">
                                <i class="fa fa-btn fa-check-square-o"></i> {{ trans('dashboard.save') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
<script>

</script>
@endsection