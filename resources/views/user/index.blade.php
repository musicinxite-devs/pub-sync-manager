@extends('layouts.default')

@section('header')
<div class="row">
    <div class="col-lg-12">
        <h2>{{ trans('dashboard.user_management') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ action('DashboardController@index') }}"><span class="nav-label">{{ trans('dashboard.dashboard') }}</span></a>
            </li>
            <li class="active">
                <strong>{{ trans('dashboard.user_management') }}</strong>
            </li>
        </ol>
    </div>
</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row">
                        <h2 class="col-md-6">{{ trans('dashboard.users') }}</h2>
                        <div class="col-md-6">
                            <a href="{!! action('UserController@getCreate') !!}" class="btn btn-primary pull-right">{{ trans('dashboard.add_new_user') }}</a>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" id="data-table">
                                    <thead>
                                    <tr>
                                        <th>{{ trans('dashboard.name') }}</th>
                                        <th>{{ trans('dashboard.email') }}</th>
                                        <th>{{ trans('dashboard.role') }}</th>
                                        <th>{{ trans('dashboard.action') }}</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
<script src="{{ asset('packages/inspinia/js/plugins/dataTables/datatables.min.js') }}"></script>
<script type="text/javascript">
    $(function() {
        $dataTable = $('#data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax:{
                url : '{!! action("UserController@postAnyData") !!}',
                type : 'POST',
                headers: {"X-CSRF-TOKEN": $('meta[name=csrf-token]').attr('content')}
            },
            columns: [
                { data: 'name', name: 'users.name' },
                { data: 'email', name: 'email' },
                { data: 'name', name: 'roles.name' },
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });

    afterDeleteSuccess = function (response) {
        if(typeof response.error != 'undefined') {
            toastr["error"](response.error, "{!! trans('dashboard.error') !!}");
        } else {
            toastr["success"]("{!! trans('dashboard.user_deleted_success_msg') !!}", "{!! trans('dashboard.success') !!}");
        }
        // Redraw grid after success
        if($dataTable !== null) {
            $dataTable.draw();
        }
    };
    afterDeleteError = function () {
        toastr["error"]("{!! trans('dashboard.user_deleted_error_msg') !!}", "{!! trans('dashboard.error') !!}");
    }
</script>
@endsection