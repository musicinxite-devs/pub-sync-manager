@extends('layouts.default')

@section('header')
<div class="row">
    <div class="col-lg-12">
        <h2>{{ trans('dashboard.device_management') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ action('DashboardController@index') }}"><span class="nav-label">{{ trans('dashboard.dashboard') }}</span></a>
            </li>
            <li class="active">
                <strong>{{ trans('dashboard.device_management') }}</strong>
            </li>
        </ol>
    </div>
</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row">
                        <h2 class="col-md-6">{{ trans('dashboard.device_detail') }}</h2>
                        <div class="col-md-6">
                            <a href="{!! action('DeviceController@getCreate',[$cust_id]) !!}" class="btn btn-primary pull-right">{{ trans('dashboard.add_new_device') }}</a>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" id="data-table">
                                    <thead>
                                    <tr>
                                        <th> UUID </th>
                                        <th>{{ trans('dashboard.login_status') }}</th>
                                        <th>{{ trans('dashboard.action') }}</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
<script src="{{ asset('packages/inspinia/js/plugins/dataTables/datatables.min.js') }}"></script>
<script type="text/javascript">
    $(function() {
        $dataTable = $('#data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{!! action("DeviceController@postAnyData",$cust_id) !!}',
                type : 'POST',
                headers: {"X-CSRF-TOKEN": $('meta[name=csrf-token]').attr('content')}
            },
            columns: [
                { data: 'uu_id', name: 'uu_id' },
                { data: 'login_status', name: 'login_status' },
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });

    afterDeleteSuccess = function (response) {
        if(typeof response.error != 'undefined') {
            toastr["error"](response.error, "{!! trans('dashboard.error') !!}");
        } else {
            toastr["success"]("{!! trans('dashboard.device_deleted_success_msg') !!}", "{!! trans('dashboard.success') !!}");
        }
        // Redraw grid after success
        if($dataTable !== null) {
            $dataTable.draw();
        }
    };
    afterDeleteError = function () {
        toastr["error"]("{!! trans('dashboard.customer_deleted_error_msg') !!}", "{!! trans('dashboard.error') !!}");
    }
</script>
@endsection