@extends('layouts.default')

@section('header_scripts')
<link href="{{ asset('packages/inspinia/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
<link href="{{ asset('packages/inspinia/css/plugins/select2/select2.min.css') }}" rel="stylesheet">

@stop

@section('header')
<div class="row">
    <div class="col-lg-12">
        <h2>{{ trans('dashboard.device_management') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ action('DashboardController@index') }}"><span class="nav-label">{{ trans('dashboard.dashboard') }}</span></a>
            </li>
            <li>
                <a href="{{ action('DeviceController@getIndex') }}"><span class="nav-label">{{ trans('dashboard.device_management') }}</span></a>
            </li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ action('DeviceController@postSave') }}" autocomplete="false">
                    {!! csrf_field() !!}
                    {{ Form::hidden('id', isset($device) ? $device->id : '') }}
                    {{ Form::hidden('customer_id', isset($device) ? $device->customer_id : $cust_id) }}

                    <div class="form-group{{ $errors->has('uu_id') ? ' has-error' : '' }}">
                        {{Form::label('uu_id', 'UUID',['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('uu_id',old('uu_id', isset($device) ? $device->uu_id : ''),['class' => 'form-control'])}}
                        </div>
                    </div>


                    <br />
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn">
                                <i class="fa fa-btn fa-check-square-o"></i> {{ trans('dashboard.save') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
