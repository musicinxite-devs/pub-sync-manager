@extends('layouts.default')

@section('header')
<div class="row">
    <div class="col-lg-12">
        <h2>{{ trans('dashboard.customer_management') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ action('DashboardController@index') }}"><span class="nav-label">{{ trans('dashboard.dashboard') }}</span></a>
            </li>
            <li class="active">
                <strong>{{ trans('dashboard.customer_management') }}</strong>
            </li>
        </ol>
    </div>
</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row">
                        <h2 class="col-md-5">{{ trans('dashboard.customers') }}</h2>
                        <div class="col-md-5">
                            <a href="{!! action('CategoryController@getList','category') !!}" class="btn btn-primary pull-right">{{ trans('dashboard.customer_category') }}</a>
                        </div>
                        <div class="col-md-1">
                            <a href="{!! action('CustomerController@getCreate') !!}" class="btn btn-primary pull-left">{{ trans('dashboard.add_new_customer') }}</a>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="row">
                                <div class="panel-body">

                                    <form role="form" class="form-inline" id="search-form" method="POST">
                                        <div class="row">
                                            <div class="form-group col-md-3">
                                                <label for="name">{{ trans('dashboard.category') }} : </label>
                                                <select id="" data-placeholder="Select Parent" class="select2 form-control" name="category_id">
                                                    <option value=""></option>
                                                    {!! $str !!}
                                                </select>
                                            </div>

                                            <div class="col-md-3">
                                                <button class="btn btn-primary form_button" id="dateSearch" type="submit">Search</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" id="data-table">
                                    <thead>
                                    <tr>
                                        <th>{{ trans('dashboard.name') }}</th>
                                        <th>{{ trans('dashboard.email') }}</th>
                                        <th>{{ trans('dashboard.phone') }}</th>
                                        <th>{{ trans('dashboard.category') }}</th>
                                        <th>{{ trans('dashboard.subscription_start_date') }}</th>
                                        <th>{{ trans('dashboard.subscription_end_date') }}</th>
                                        <th>{{ trans('dashboard.action') }}</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
<script src="{{ asset('packages/inspinia/js/plugins/dataTables/datatables.min.js') }}"></script>
<script type="text/javascript">
    $(function() {
        $dataTable = $('#data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                url: '{!! action("CustomerController@postAnyData") !!}',
                data: function (d) {
                    d.category_id = $('select[name=category_id]').val();
                },
                type : 'POST',
                headers: {"X-CSRF-TOKEN": $('meta[name=csrf-token]').attr('content')}
            },
            oLanguage: {
                sLengthMenu: "_MENU_",
                sSearch: ""
            },
            columns: [
                { data: 'name', name: 'customers.name' },
                { data: 'email', name: 'customers.email' },
                { data: 'phone', name: 'customers.phone' },
                { data: 'category_title', name: 'categories.title' },
                { data: 'subscription_start_date', name: 'customers.subscription_start_date' },
                { data: 'subscription_end_date', name: 'customers.subscription_end_date' },
                { data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
        $('#search-form').on('submit', function(e) {
            $dataTable.draw();
            e.preventDefault();
        });
    });

    afterDeleteSuccess = function (response) {
        if(typeof response.error != 'undefined') {
            toastr["error"](response.error, "{!! trans('dashboard.error') !!}");
        } else {
            toastr["success"]("{!! trans('dashboard.customer_deleted_success_msg') !!}", "{!! trans('dashboard.success') !!}");
        }
        // Redraw grid after success
        if($dataTable !== null) {
            $dataTable.draw();
        }
    };
    afterDeleteError = function () {
        toastr["error"]("{!! trans('dashboard.customer_deleted_error_msg') !!}", "{!! trans('dashboard.error') !!}");
    }
</script>
@endsection