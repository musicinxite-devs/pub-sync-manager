@extends('layouts.default')

@section('header_scripts')
<link href="{{ asset('packages/inspinia/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
<link href="{{ asset('packages/inspinia/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
<style type="text/css">
    .stream-row {
        padding: 10px;
    }
</style>
@stop

@section('header')
<div class="row">
    <div class="col-lg-12">
        <h2>{{ trans('dashboard.customer_management') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ action('DashboardController@index') }}"><span class="nav-label">{{ trans('dashboard.dashboard') }}</span></a>
            </li>
            <li>
                <a href="{{ action('CustomerController@getIndex') }}"><span class="nav-label">{{ trans('dashboard.customer_management') }}</span></a>
            </li>
            <li class="active">
                <strong>{{ isset($customer) ? trans('dashboard.update_customer') : trans('dashboard.add_customer') }}</strong>
            </li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ action('CustomerController@postSave') }}" autocomplete="false">
                    {!! csrf_field() !!}
                    {{ Form::hidden('id', isset($customer) ? $customer->id : '') }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {{Form::label('name', trans('dashboard.name'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('name',old('name', isset($customer) ? $customer->name : ''),['class' => 'form-control'])}}

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {{Form::label('email', trans('dashboard.email'),['class' => 'col-md-4 control-label'])}}

                        <div class="col-md-6">
                            {{Form::email('email',old('email', isset($customer) ? $customer->email : ''),['class' => 'form-control'])}}

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                        {{Form::label('phone', trans('dashboard.phone'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('phone',old('phone', isset($customer) ? $customer->phone : ''),['class' => 'form-control'])}}

                            @if ($errors->has('phone'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('phone') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row @if ($errors->has('category_id')) has-error has-feedback @endif">
                        <label class="col-md-4 control-label text-md-right" for="disabled-input">{{ trans('dashboard.category') }}</label>
                        <div class="col-md-3">

                            <select id="multiple-select" data-placeholder="Select Category" class="select2 form-control"
                                    data-placeholder = 'Assign clients' name="category_id">
                                {!! $str !!}
                            </select>

                            @if ($errors->has('category_id'))
                                <span class="help-block">
                                        <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>


                    <div class="form-group{{ $errors->has('subscription_start_date') ? ' has-error' : '' }}">
                        {{Form::label('subscription_start_date', trans('dashboard.subscription_start_date'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('subscription_start_date',old('name', isset($customer) ? date(config('music-inxite.common.dateformat.php'),strtotime($customer->subscription_start_date)) : $date),['class' => 'form-control'])}}

                            @if ($errors->has('subscription_start_date'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('subscription_start_date') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('subscription_end_date') ? ' has-error' : '' }}">
                        {{Form::label('subscription_end_date', trans('dashboard.subscription_end_date'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('subscription_end_date',old('subscription_end_date', isset($customer) ? date(config('music-inxite.common.dateformat.php'),strtotime($customer->subscription_end_date)) : $date),['class' => 'form-control'])}}

                            @if ($errors->has('subscription_end_date'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('subscription_end_date') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                        {{Form::label('password', trans('dashboard.password'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::password('password', ['class' => 'form-control'])}}

                            @if ($errors->has('password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('pin') ? ' has-error' : '' }}">
                        {{Form::label('pin', trans('dashboard.pin'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('pin',old('pin', isset($customer) ? $customer->pin : ''), ['class' => 'form-control'])}}

                            @if ($errors->has('pin'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('pin') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('removal_request_email') ? ' has-error' : '' }}">
                        {{Form::label('removal_request_email', trans('dashboard.removal_request_email'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('removal_request_email',old('removal_request_email', isset($customer) ? $customer->removal_request_email : ''), ['class' => 'form-control'])}}

                            @if ($errors->has('removal_request_email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('removal_request_email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('max_concurrrent_login') ? ' has-error' : '' }}">
                        {{Form::label('password', trans('dashboard.max_login'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('max_concurrent_login',old('max_concurrent_login', isset($customer) ? $customer->max_concurrent_login : ''), ['class' => 'form-control'])}}

                            @if ($errors->has('max_concurrent_login'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('max_concurrent_login') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>




                    <div class="form-group streams-container">
                        {{Form::label('streams', trans('dashboard.streams'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            <div class="row">
                                @if(isset($customer))
                                    @if(old('stream'))
                                        @foreach(old('stream') as $key => $stream_id)
                                        <div class="col-md-12 stream-row">
                                            <div class="row">
                                                <div class="col-xs-11">

                                                    <select class="form-control select-chosen" name="stream[]">

                                                        @foreach($stream_arr as $id=>$title)
                                                            <option value="{{ $id }}" {{ ( $id == $stream_id )? 'selected' : ''}}> {{ $title }}  </option>
                                                        @endforeach
                                                    </select>

                                                </div>
                                                <a href="javascript:void(0);" class="col-xs-1"><i class="fa fa-btn fa-remove remove-stream"></i></a>
                                            </div>
                                        </div>
                                        @endforeach

                                    @else
                                        @foreach($customer->streams as $customerStream)
                                        <div class="col-md-12 stream-row">
                                            <div class="row">
                                                <div class="col-xs-11">
                                                    {{Form::select('stream',$streams->lists('title', 'id')->toArray(), $customerStream->id, ['class' => 'form-control select-chosen', "name" => "stream[]"])}}
                                                </div>
                                                <a href="javascript:void(0);" class="col-xs-1"><i class="fa fa-btn fa-remove remove-stream"></i></a>
                                            </div>
                                        </div>
                                        @endforeach
                                     @endif
                                @endif
                                <button type="button" class="btn add-stream">{{ trans('dashboard.add_stream') }}</button>
                            </div>
                        </div>
                    </div>

                    <br />
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn">
                                <i class="fa fa-btn fa-check-square-o"></i> {{ trans('dashboard.save') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
<script src="{{ asset('packages/inspinia/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('packages/inspinia/js/plugins/select2/select2.full.min.js') }}"></script>
<script id="select-stream-template" type="x-tmpl">
    <div class="col-md-12 stream-row">
        <div class="row">
            <div class="col-xs-11">
                {{Form::select('stream',$streams->lists('title', 'id')->toArray(), '', ['class' => 'col-md-10 form-control select-chosen', "name" => "stream[]"])}}
            </div>
            <a href="javascript:void(0);" class="col-xs-1"><i class="fa fa-btn fa-remove remove-stream"></i></a>
        </div>
    </div>
</script>

<script>
    $('#subscription_start_date, #subscription_end_date').datepicker({format: "{{ config('music-inxite.common.dateformat.js') }}"});
    $('#subscription_start_date, #subscription_end_date').prop('readonly','true');

    function checkStreamRemove() {
        if($(".remove-stream").length == 1) {
            $(".remove-stream").hide();
        } else {
            $(".remove-stream").show();
        }
    }

    $(".add-stream").on("click", function (e) {
        e.preventDefault();
        $(this).before($("#select-stream-template").html());
        setTimeout(function() {
            $("select.select-chosen:visible").select2();
        }, 300);
        checkStreamRemove();
    });

    $(document).on("click", ".remove-stream", function (e) {
        e.preventDefault();
        $(this).closest(".stream-row").remove();
        checkStreamRemove();
    });

//        $("#subscription_start_date").current
//    $("#subscription_start_date").on("change", function() {
//        if($(this).datepicker('getDate') > $("#subscription_end_date").datepicker('getDate')) {
//            $('#subscription_end_date').datepicker('update', '');
//        }
//        $("#subscription_end_date").datepicker('setStartDate', $(this).datepicker('getDate'));
//    }).trigger("change");
    checkStreamRemove();
</script>
@endsection