@extends('layouts.default')

@section('header_scripts')
<link href="{{ asset('packages/inspinia/css/plugins/dropzone/dropzone.css') }}" rel="stylesheet">
<style type="text/css">
    .file-container {
        border: 1px solid #e5e6e7;
        padding: 6px 12px;
        font-size: 14px;
    }
</style>
@stop

@section('header')
<div class="row">
    <div class="col-lg-12">
        <h2>{{ trans('dashboard.track_management') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ action('DashboardController@index') }}"><span class="nav-label">{{ trans('dashboard.dashboard') }}</span></a>
            </li>
            <li>
                <a href="{{ action('TrackController@getIndex') }}"><span class="nav-label">{{ trans('dashboard.track_management') }}</span></a>
            </li>
            <li class="active">
                <strong>{{ trans('dashboard.add_track') }}</strong>
            </li>
        </ol>
    </div>
</div>
<meta name="csrf-token" content="{!! csrf_token() !!}">
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal" name ="track_upload" role="form" method="POST" action="{{ action('TrackController@postSave') }}" autocomplete="false">
                    {!! csrf_field() !!}
                    <div class="file-container">

                    </div>

                    <br />
                    <div class="track-file dropzone dropzone-mini">
                        <div class="fallback">
                            {{ Form::file('track', null, ['class' => 'form-control', 'multiple' => 'multiple']) }}
                        </div>
                    </div>

                    <br />
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="button" class="btn submit" onclick="peventDuplicateTrack();">
                                <i class="fa fa-btn fa-check-square-o"></i> {{ trans('dashboard.save') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
<script src="{{ asset('packages/inspinia/js/plugins/dropzone/dropzone.js') }}"></script>
<script id="file-content-template" type="x-tmpl">
    <div class="row">
        <div class="form-group">
            {{ Form::label('title', trans('dashboard.title'),['class' => 'col-md-4 control-label']) }}
             {!! csrf_field() !!}
            <div class="col-md-6">
                <input type="text" name="title[]" value="" class="title" />
                    <span class="help-block alert-danger" id="title_error[]" style="display:none">
                        <strong> <label class="track_exist"> </label> </strong>
                    </span>
            </div>
        </div>
        <div class="form-group">
            {{ Form::label('artist', trans('dashboard.artist'),['class' => 'col-md-4 control-label']) }}
            <div class="col-md-6">
                <input type="text" name="artist[]" value="" class="artist" />

            </div>
        </div>
        <div class="form-group">
            {{ Form::label('album', trans('dashboard.album'),['class' => 'col-md-4 control-label']) }}
            <div class="col-md-6">
                <input type="text" name="album[]" value="" class="album" />
                <input type="hidden" name="file_path[]" value="" class="file-path" />
            </div>
        </div>
        <div class="col-xs-1"><a href="javascript:void(0)" class="btn btn-danger remove-file">{{ trans('dashboard.remove') }}</a></div>
    </div>
</script>
<script type="text/javascript">
    Dropzone.autoDiscover = false;
    $(document).ready(function() {
        $(".music-files").find(".hidden").hide().removeClass("hidden");
        var csrf_token   =   $('meta[name="csrf-token"]').attr('content');
        $(".track-file").dropzone({
            url: "{{ URL::action('TrackController@postUpload') }}",
            paramName: 'track-file',
            method: "post",
            uploadMultiple: true,
            parallelUploads: 1,
            headers: {"X-CSRF-TOKEN": csrf_token},
            addRemoveLinks : true,
            acceptedFiles: 'audio/basic,audio/mpeg,audio/x-mpeg,audio/x-wav,audio/x-ms-wma,audio/x-qt-stream,audio/x-ms-wma,audio/voxware,audio/midi,audio/mp3,audio/ogg,audio/x-m4a,audio/x-realaudio',
            success: function (file, response) {

                $('#title_error').css('display','block');
                if(typeof response.status != 'undefined' && response.status == 200) {
                    // response.file_path
                    var $element = $($("#file-content-template").html());
                    $element.find(".title").val(response.title);
                    $element.find(".artist").val(response.artist);
                    $element.find(".album").val(response.album);
                    $element.find(".file-path").val(response.file_path);

                    if(response.track_exist != '') {
                        $element.find(".help-block").css('display','block');
                        $element.find(".track_exist").text(response.track_exist);
                    }

                    $(".file-container").append($element);
                    $(".file-container").show();
                    this.removeFile(this.files[0]);
                } else if(typeof response.message != 'undefined') {

                    toastr["error"](response.message, "{!! trans('dashboard.error') !!}");
                }
            },
            complete: function () {
                $(".submit").prop("disabled", false );
            },
            sending: function(file, xhr, formData) {
                formData.append("_token", "{{ csrf_token() }}");
                $(".submit").prop("disabled", true);
            }
        });
        $(document).on("click", ".remove-file",function (e) {
            e.preventDefault()
            $(this).closest(".row").remove();
        });
        $(document).on("click", ".cancel-file-upload",function (e) {
            e.preventDefault()
            $(".file-container").show();
            $(".track-file, .cancel-file-upload").hide();
        });
    });

    function peventDuplicateTrack() {

        var url;
        url = '{!! action("TrackController@postDuplicateTrack") !!}';
        data = $('form').serialize();

        $.ajax({
            url: url,
            headers: {"X-CSRF-TOKEN": $('input[name=_token]').val()},
            data: data,
            type: 'POST',
            dataType: 'JSON',
            success: function (response) {
                console.log(response);
                if(response.status == 200) {
                    track_upload.submit();
                }

            }
        });
    }
</script>
@endsection