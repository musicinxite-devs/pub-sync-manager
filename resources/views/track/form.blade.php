@extends('layouts.default')

@section('header_scripts')
<link href="{{ asset('packages/inspinia/css/plugins/dropzone/dropzone.css') }}" rel="stylesheet">
<link href="{{ asset('packages/inspinia/css/plugins/parsley/application.min.css') }}" rel="stylesheet">
<style type="text/css">
    .file-container {
        border: 1px solid #e5e6e7;
        padding: 6px 12px;
        font-size: 14px;
    }
</style>
@stop

@section('header')
<div class="row">
    <div class="col-lg-12">
        <h2>{{ trans('dashboard.track_management') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ action('DashboardController@index') }}"><span class="nav-label">{{ trans('dashboard.dashboard') }}</span></a>
            </li>
            <li>
                <a href="{{ action('TrackController@getIndex') }}"><span class="nav-label">{{ trans('dashboard.track_management') }}</span></a>
            </li>
            <li class="active">
                <strong>{{ trans('dashboard.update_track') }}</strong>
            </li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ action('TrackController@postUpdate') }}" autocomplete="false">
                    {!! csrf_field() !!}
                    {{ Form::hidden('id', isset($track) ? $track->id : '') }}
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        {{Form::label('title', trans('dashboard.title'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('title',old('title', isset($track) ? $track->title : ''),['class' => 'form-control'])}}

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('artist') ? ' has-error' : '' }}">
                        {{Form::label('artist', trans('dashboard.artist'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('artist',old('artist', isset($track) ? $track->artist : ''),['class' => 'form-control'])}}

                            @if ($errors->has('artist'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('artist') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('album') ? ' has-error' : '' }}">
                        {{Form::label('album', trans('dashboard.album'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('album',old('album', isset($track) ? $track->album : ''),['class' => 'form-control'])}}

                            @if ($errors->has('album'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('album') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row @if ($errors->has('category_id')) has-error has-feedback @endif">
                        <label class="col-md-4 control-label text-md-right" for="disabled-input">{{ trans('dashboard.category') }}</label>
                        <div class="col-md-3">

                            <select id="multiple-select" data-placeholder="Select Category" class="select2 form-control"
                                    data-placeholder = 'Assign clients' multiple name="category_id[]">
                                <option value=""></option>
                                {!! $str !!}
                            </select>

                            @if ($errors->has('category_id'))
                                <span class="help-block">
                                            <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                            @endif
                        </div>

                    </div>
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }} music-files">
                        {{Form::label('music_file', trans('dashboard.music_file'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            @if(isset($track))
                                <div class="file-container">
                                    <div class="row">
                                        <div class="col-xs-11 div-file-name">
                                            {{ $track->file_path }}
                                        </div>
                                        <div class="col-xs-1"><a href="javascript:void(0)" class="change-file"><i class="fa fa-refresh"></i></i></a></div>
                                    </div>
                                </div>
                                <button type="button" class="btn cancel-file-upload hidden">
                                    {{ trans('dashboard.cancel') }}
                                </button>
                                <div class="track-file dropzone dropzone-mini hidden">
                                    <div class="fallback">
                                        {{ Form::file('track', null, ['class' => 'form-control']) }}
                                    </div>
                                    {{Form::hidden('file_path', $track->file_path, ['id'=>'file_path'])}}
                                </div>
                            @else
                                <div class="file-container hidden"></div>
                                <button type="button" class="btn cancel-file-upload hidden">
                                    {{ trans('dashboard.cancel') }}
                                </button>
                                <div class="track-file dropzone dropzone-mini">
                                    <div class="fallback">
                                        {{ Form::file('track', null, ['class' => 'form-control']) }}
                                    </div>
                                    {{Form::hidden('file_path', '', ['id'=>'file_path'])}}
                                </div>
                            @endif

                        </div>
                    </div>

                    <br />
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn">
                                <i class="fa fa-btn fa-check-square-o"></i> {{ trans('dashboard.save') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
<script src="{{ asset('packages/inspinia/js/plugins/dropzone/dropzone.js') }}"></script>
<script src="{{asset('packages/inspinia/js/plugins/parsleyjs/dist/parsley.min.js')}}"></script>
<script src="{{asset('packages/inspinia/js/plugins/select2/select2.min.js')}}"></script>
<script id="file-content-template" type="x-tmpl">
    <div class="row">
        <div class="col-xs-11 div-file-name">

        </div>
        <div class="col-xs-1"><a href="javascript:void(0)" class="change-file"><i class="fa fa-refresh"></i></a></div>
    </div>
</script>
<script type="text/javascript">
    Dropzone.autoDiscover = false;
    $(document).ready(function() {
        $(".music-files").find(".hidden").hide().removeClass("hidden");

        $(".track-file").dropzone({
            url: "{{ URL::action('TrackController@postUpload') }}",
            maxFiles: 1,
            paramName: 'track-file',
            addRemoveLinks : true,
            acceptedFiles: 'audio/basic,audio/mpeg,audio/x-mpeg,audio/x-wav,audio/x-ms-wma,audio/x-qt-stream,audio/x-ms-wma,audio/voxware,audio/midi,audio/mp3,audio/ogg,audio/x-m4a,audio/x-realaudio',
            success: function (file, response) {
                if(typeof response.status != 'undefined' && response.status == 200) {
                    $("#file_path").val(response.file_path);
                    $(".file-container").html($("#file-content-template").html()).find(".div-file-name").html(response.file_path);
                    $(".file-container").show();
                    $(".track-file").hide();
                    this.removeFile(this.files[0]);
                } else if(typeof response.message != 'undefined') {
                    toastr["error"](response.message, "{!! trans('dashboard.error') !!}");
                }
            },
            complete: function () {
             $(":submit").prop("disabled", false );
            },
            sending: function(file, xhr, formData) {
                formData.append("_token", $('[name="_token"]').val());
                $(":submit").prop("disabled", true);
            }
        });
        $(document).on("click", ".change-file",function (e) {
            e.preventDefault()
            $(".file-container").hide();
            $(".track-file, .cancel-file-upload").show();
        });
        $(document).on("click", ".cancel-file-upload",function (e) {
            e.preventDefault()
            $(".file-container").show();
            $(".track-file, .cancel-file-upload").hide();
        });

        $(".select2").each(function(){
            $(this).select2($(this).data());
        });
    });


</script>
@endsection