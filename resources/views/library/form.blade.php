@extends('layouts.default')

@section('header_scripts')
<link href="{{ asset('packages/inspinia/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
<style type="text/css">
    .track-row {
        padding: 10px;
    }
</style>
@stop

@section('header')
<div class="row">
    <div class="col-lg-12">
        <h2>{{ trans('dashboard.library_management') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ action('DashboardController@index') }}"><span class="nav-label">{{ trans('dashboard.dashboard') }}</span></a>
            </li>
            <li>
                <a href="{{ action('LibraryController@getIndex') }}"><span class="nav-label">{{ trans('dashboard.library_management') }}</span></a>
            </li>
            <li class="active">
                <strong>{{ isset($library) ? trans('dashboard.update_library') : trans('dashboard.add_library') }}</strong>
            </li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ action('LibraryController@postSave') }}" autocomplete="false">
                    {!! csrf_field() !!}
                    {{ Form::hidden('id', isset($library) ? $library->id : '') }}
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        {{Form::label('title', trans('dashboard.title'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('title',old('title', isset($library) ? $library->title : ''),['class' => 'form-control'])}}

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row @if ($errors->has('category_id')) has-error has-feedback @endif">
                        <label class="col-md-4 control-label text-md-right" for="disabled-input">{{ trans('dashboard.category') }}</label>
                        <div class="col-md-3">

                            <select id="multiple-select" data-placeholder="Select Category" class="select2 form-control"
                                    data-placeholder = 'Assign clients' name="category_id">
                                {!! $str !!}
                            </select>

                            @if ($errors->has('category_id'))
                                <span class="help-block">
                                            <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="tracks-container">
                        {{ Form::label('tracks', trans('dashboard.tracks'),['class' => 'col-md-4 control-label']) }}
                        <div class="col-md-6">
                            <div class="row">
                                @if (isset($library))
                                @foreach($library->tracks as $libraryTrack)
                                <div class="col-md-12 track-row">
                                    <div class="row">
                                        <div class="col-xs-11">
                                            {{Form::select('track',$tracks, $libraryTrack->id, ['class' => 'form-control select-chosen', 'name'=> 'track[]'])}}
                                        </div>
                                        <a href="javascript:void(0);" class="col-xs-1 remove-track"><i class="fa fa-btn fa-remove"></i></a>
                                    </div>
                                </div>
                                @endforeach
                                @endif
                                <button type="button" class="btn add-track">{{ trans('dashboard.add_track') }}</button>
                            </div>
                        </div>
                    </div>

                    <br />
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn">
                                <i class="fa fa-btn fa-check-square-o"></i> {{ trans('dashboard.save') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
<script src="{{ asset('packages/inspinia/js/plugins/select2/select2.full.min.js') }}"></script>
<script id="select-track-template" type="x-tmpl">
    <div class="col-md-12 track-row">
        <div class="row">
            <div class="col-xs-11">
                {{Form::select('track',$tracks, '', ['class' => 'form-control select-chosen', 'name'=> 'track[]'])}}
            </div>
            <a href="javascript:void(0);" class="col-xs-1 remove-track"><i class="fa fa-btn fa-remove"></i></a>
        </div>
    </div>
</script>

<script type="text/javascript">
    function checkTrackRemove() {
        if($(".remove-track").length == 1) {
            $(".remove-track").hide();
        } else {
            $(".remove-track").show();
        }
    }
    $(document).ready(function() {
        $(".add-track").on("click", function (e) {
            e.preventDefault();
            $(this).before($("#select-track-template").html());
            setTimeout(function() {
                $("select.select-chosen:visible").select2();
            }, 300);
            checkTrackRemove();
        });

        $(document).on("click", ".remove-track", function (e) {
            e.preventDefault();
            $(this).closest(".track-row").remove();
            checkTrackRemove();
        });
        checkTrackRemove();
    });
</script>
@endsection