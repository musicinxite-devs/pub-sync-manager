@extends('layouts.default')

@section('header')
    <div class="row">
        <div class="col-lg-12">
            <h2>{{ trans('dashboard.category_management') }}</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{ action('DashboardController@index') }}"><span class="nav-label">{{ trans('dashboard.dashboard') }}</span></a>
                </li>
                <li class="active">
                    <strong>{{ trans('dashboard.category_management') }}</strong>
                </li>
            </ol>
        </div>
    </div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row">
                        <h2 class="col-md-6">{{ trans('dashboard.categories') }}</h2>
                        <div class="col-md-6">
                            <a href="{!! action('CategoryController@getCreate',$type) !!}" class="btn btn-primary pull-right">{{ trans('dashboard.add_new_cat') }}</a>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div id="nestable-menu">
                            <button type="button" data-action="expand-all" class="btn btn-white btn-sm">Expand All</button>
                            <button type="button" data-action="collapse-all" class="btn btn-white btn-sm">Collapse All</button>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6">
                        <div class="ibox-content">
                            @if(isset($catArr))
                            <div class="dd" id="nestable">

                                <ol class="dd-list">{!! $str !!}
                                </ol>
                            </div>
                            @else
                                {{ trans('dashboard.no_category') }}
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script src="{{ asset('packages/inspinia/js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('packages/inspinia/js/plugins/nestable/jquery.nestable.js') }}"></script>
    <script type="text/javascript">

        $(function(){

            var updateOutput = function (e) {
                var list = e.length ? e : $(e.target),
                        output = list.data('output');
                if (window.JSON) {
                    output.val(window.JSON.stringify(list.nestable('serialize')));//, null, 2));
                } else {
                    output.val('JSON browser support required for this demo.');
                }
            };
            // activate Nestable for list 1
            $('#nestable').nestable({
                group: 1
            }).on('change', updateOutput);


           //  output initial serialised data
            updateOutput($('#nestable').data('output', $('#nestable-output')));

            $('#nestable-menu').on('click', function (e) {
                var target = $(e.target),
                        action = target.data('action');
                if (action === 'expand-all') {
                    $('.dd').nestable('expandAll');
                }
                if (action === 'collapse-all') {
                    $('.dd').nestable('collapseAll');
                }
            });
    });
    </script>
@endsection