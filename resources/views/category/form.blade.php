@extends('layouts.default')

@section('header_scripts')
<link href="{{ asset('packages/inspinia/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
<link href="{{ asset('packages/inspinia/css/plugins/select2/select2.min.css') }}" rel="stylesheet">

@stop

@section('header')
<div class="row">
    <div class="col-lg-12">
        <h2>{{ trans('dashboard.category_management') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ action('DashboardController@index') }}"><span class="nav-label">{{ trans('dashboard.dashboard') }}</span></a>
            </li>
            <li>
                <a href="{{ action('CategoryController@getList',$type) }}"><span class="nav-label">{{ trans('dashboard.category_management') }}</span></a>
            </li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ action('CategoryController@postSave') }}" autocomplete="false">
                    {!! csrf_field() !!}
                    {{ Form::hidden('id', isset($category) ? $category->id : '') }}
                    {{ Form::hidden('type', $type) }}

                    @if (session('category'))
                        <div class="alert alert-danger">
                            {{ session('category') }}
                        </div>
                    @endif
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        {{Form::label('title', trans('dashboard.title'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('title',old('title', isset($category) ? $category->title : ''),['class' => 'form-control'])}}
                        </div>

                        @if ($errors->has('title'))
                            <span class="help-block">
                                            <strong>{{ $errors->first('title') }}</strong>
                                    </span>
                        @endif
                    </div>

                    <div class="form-group row @if ($errors->has('parent_id')) has-error has-feedback @endif">
                        <label class="col-md-4 control-label text-md-right" for="disabled-input">{{ trans('dashboard.parent_category') }}</label>
                        <div class="col-md-3">

                            <select id="" data-placeholder="Select Parent" class="select2 form-control" name="parent_id">
                                <option value=""></option>
                                {!! $str !!}
                            </select>

                            @if ($errors->has('parent_id'))
                                <span class="help-block">
                                            <strong>{{ $errors->first('parent_id') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4 control-label text-md-right" for="disabled-input">Status</label>
                        <div class="col-md-2">
                            {!!form::select('status',['Y'=>'Active','N'=>'Inactive'],isset($category->id) ? $category->status: "",['class'=>'select2 form-control','id'=>'default-select'])!!}
                            @if ($errors->has('status'))
                                <span class="help-block alert-danger">
                                            <strong>{{ $errors->first('status') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>

                    <br />

                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn">
                                <i class="fa fa-btn fa-check-square-o"></i> {{ trans('dashboard.save') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
