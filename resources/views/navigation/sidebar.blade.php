@foreach($items as $item)
    <li class="{{ $item->attr("class") }}">
        <a href="{{ $item->url() }}">
            {!! $item->title !!}
            @if($item->hasChildren() && $item->data('openable'))
                <span class="fa arrow"></span>
            @endif
        </a>
        @if($item->hasChildren() && $item->data('openable'))
            <ul class="nav nav-{{ (isset($level) ? $level : 'second') }}-level collapse">
                @include('navigation.sidebar', array('items' => $item->children(), 'level' => 'third'))
            </ul>
        @endif
    </li>
@endforeach