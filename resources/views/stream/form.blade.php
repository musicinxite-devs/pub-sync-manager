@extends('layouts.default')

@section('header_scripts')
<link href="{{ asset('packages/inspinia/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
@stop

@section('header')
<div class="row">
    <div class="col-lg-12">
        <h2>{{ trans('dashboard.stream_management') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ action('DashboardController@index') }}"><span class="nav-label">{{ trans('dashboard.dashboard') }}</span></a>
            </li>
            <li>
                <a href="{{ action('StreamController@getIndex') }}"><span class="nav-label">{{ trans('dashboard.stream_management') }}</span></a>
            </li>
            <li class="active">
                <strong>{{ isset($stream) ? trans('dashboard.update_stream') : trans('dashboard.add_stream') }}</strong>
            </li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ action('StreamController@postSave') }}" autocomplete="false">
                    {!! csrf_field() !!}
                    {{ Form::hidden('id', isset($stream) ? $stream->id : '') }}
                    <div class="form-group{{ $errors->has('title') ? ' has-error' : '' }}">
                        {{Form::label('title', trans('dashboard.title'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('title',old('title', isset($stream) ? $stream->title : ''),['class' => 'form-control'])}}

                            @if ($errors->has('title'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('title') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('url') ? ' has-error' : '' }}">
                        {{Form::label('url', trans('dashboard.url'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('url',old('url', isset($stream) ? $stream->url : ''),['class' => 'form-control'])}}

                            @if ($errors->has('url'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('url') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group tracks-container">
                        {{Form::label('library_id', trans('dashboard.library'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::select('library_id',$libraries->lists('title', 'id')->toArray(), isset($stream) ? $stream->library_id : '', ['class' => 'form-control select-chosen'])}}

                            @if ($errors->has('library'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('library') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row @if ($errors->has('category_id')) has-error has-feedback @endif">
                        <label class="col-md-4 control-label text-md-right" for="disabled-input">{{ trans('dashboard.category') }}</label>
                        <div class="col-md-3">

                            <select id="multiple-select" data-placeholder="Select Category" class="select2 form-control"
                                   name="category_id">
                                {!! $str !!}
                            </select>

                            @if ($errors->has('category_id'))
                                <span class="help-block">
                                            <strong>{{ $errors->first('category_id') }}</strong>
                                    </span>
                            @endif
                        </div>
                    </div>
                    <br />
                    <div class="form-group">
                        <div class="col-md-12 text-center">
                            <button type="submit" class="btn">
                                <i class="fa fa-btn fa-check-square-o"></i> {{ trans('dashboard.save') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
<script src="{{ asset('packages/inspinia/js/plugins/select2/select2.full.min.js') }}"></script>

<script type="text/javascript">
    Dropzone.autoDiscover = false;

    $(document).ready(function() {
        $("select.select-chosen").select2();
    });
</script>
@endsection