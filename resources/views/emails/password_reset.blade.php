@extends('layouts.email.default')
@section('title_content')
<h1>Reset Password</h1>
@endsection

@section('content')
Hello,
<br><br>
We're sorry you had trouble logging in. Your new password for logging in is: {{$password}}
<br><br>
Thanks,<br>
Music Inxite
@endsection