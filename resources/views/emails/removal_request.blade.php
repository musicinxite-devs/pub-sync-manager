@extends('layouts.email.default')
@section('title_content')
    <h1>Track Removal/More like Request</h1>
@endsection

@section('content')
    Hello,
    <br><br>
    {{$title}} from the user {{ $customer->name }}.
    <br><br>
    Stream : {{ $stream->title }}<br>
    Date / time : {{ $date }}<br>
    Track meta data: {{ $track_removal->meta_data }}

    <br><br>
    Thanks,<br>
    Music Inxite
@endsection
