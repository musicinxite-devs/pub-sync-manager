@include('layouts.header')
    <div class="row wrapper border-bottom white-bg page-heading">
        @yield('header')
    </div>
    <div class="wrapper wrapper-content animated fadeInRight">
        @yield('content')
    </div>
@include('layouts.footer')