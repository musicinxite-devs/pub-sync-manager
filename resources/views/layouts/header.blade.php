<!DOCTYPE html>
<html lang="{{Lang::getLocale()}}">
<head>

    <!-- Metadata -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>
        @section('title')
        Administration
        @show
    </title>

    <!-- CSS -->
    <link href="{{ asset('packages/inspinia/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/inspinia/css/plugins/awesome-bootstrap-checkbox/awesome-bootstrap-checkbox.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/inspinia/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/inspinia/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/inspinia/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/inspinia/css/plugins/dataTables/datatables.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/inspinia/css/style.css') }}" rel="stylesheet">
    @yield('styles')

    <script type="text/javascript">
        var csrfToken = '{!! csrf_token() !!}';
    </script>

    @yield('header_scripts')
</head>

<body class="no-skin-config">
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element">
                        <a href="/" class="btn btn-block btn-primary">Music Inxite</a>
                    </div>
                    <div class="logo-element">
                        <a href="/" class="btn btn-primary">MI</a>
                    </div>
                </li>
                @include('navigation.sidebar', array('items' => $frontendNavigation->roots()))
            </ul>
        </div>
    </nav>
    <div id="page-wrapper" class="gray-bg dashbard-1">
        <div class="row border-bottom">
            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
                </div>
                <ul class="nav navbar-top-links navbar-right">
                    <li>
                        <span class="m-r-sm text-muted welcome-message">Welcome {{ Auth::user()->name }}!</span>
                    </li>
                    <li>
                        <a href="{{ url('/logout') }}">
                            <i class="fa fa-sign-out"></i> Log out
                        </a>
                    </li>
                </ul>
        </div>