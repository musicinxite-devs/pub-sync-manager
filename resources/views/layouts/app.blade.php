<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Login - Music Inxite</title>

    <!-- CSS -->
    <link href="{{ asset('packages/inspinia/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/inspinia/font-awesome/css/font-awesome.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/inspinia/css/animate.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/inspinia/css/style.css') }}" rel="stylesheet">
    <link href="{{ asset('css/custom-front.css') }}" rel="stylesheet">

    @yield('styles')
    @yield('header_scripts')
</head>
<body id="page-top" class="landing-page no-skin-config">

    @yield('content')

    <div class="mi-background" data-sr-id="5" style=" ">
    </div>
    @yield('footer_scripts')
</body>
</html>
