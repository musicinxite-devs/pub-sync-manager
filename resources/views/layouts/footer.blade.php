<div class="footer" >
    <div>
        <strong>Copyright</strong> Music Inxite &copy; 2016
    </div>
</div>
</div>
</div>

<!-- Mainly scripts -->
<script src="{{ asset('packages/inspinia/js/jquery-2.1.1.js') }}"></script>
<script src="{{ asset('packages/inspinia/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('packages/inspinia/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('packages/inspinia/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ asset('packages/inspinia/js/inspinia.js') }}"></script>
<script src="{{ asset('packages/inspinia/js/plugins/pace/pace.min.js') }}"></script>

<!-- jQuery UI -->
<script src="{{ asset('packages/inspinia/js/plugins/jquery-ui/jquery-ui.min.js') }}"></script>

<!-- Toastr -->
<script src="{{ asset('packages/inspinia/js/plugins/toastr/toastr.min.js') }}"></script>

<!-- Jquery Validate -->
<script src="{{ asset('packages/inspinia/js/plugins/validate/jquery.validate.min.js') }}"></script>

<script src="{{ asset('js/application.js') }}"></script>

@yield('footer_scripts')

<script type="text/javascript">
    $(document).ready(function () {
        toastr.options = {
            closeButton: true,
            progressBar: true,
            showMethod: 'slideDown',
            timeOut: 4000
        };

        @if(session('success'))
            toastr.success("{!! session('success') !!}", "{!! trans('dashboard.success') !!}");
        @endif
        @if(session('error'))
            toastr.error("{!! session('error') !!}", "{!! trans('dashboard.error') !!}");
        @endif
    });
</script>
</body>
</html>