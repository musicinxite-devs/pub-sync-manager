@extends('layouts.default')

@section('header')
<div class="row">
    <div class="col-lg-12">
        <h2>{{ trans('dashboard.user_roles') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ action('DashboardController@index') }}"><span class="nav-label">{{ trans('dashboard.dashboard') }}</span></a>
            </li>
            <li class="active">
                <strong>{{ trans('dashboard.user_roles') }}</strong>
            </li>
        </ol>
    </div>
</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row">
                        <h2 class="col-md-6">{{ trans('dashboard.roles') }}</h2>
                        <div class="col-md-6">
                            <a href="{!! action('RoleController@getCreate') !!}" class="btn btn-primary pull-right">{{ trans('dashboard.add_new_role') }}</a>
                        </div>
                    </div>
                </div>
                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" id="data-table">
                                    <thead>
                                    <tr>
                                        <th>{{ trans('dashboard.roles') }}</th>
                                        <th>{{ trans('dashboard.action') }}</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
<script src="{{ asset('packages/inspinia/js/plugins/dataTables/datatables.min.js') }}"></script>
<script>
    $(function() {
        $dataTable = $('#data-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! action("RoleController@anyData") !!}',
            columns: [
                { data: 'name', name: 'name' },
                {data: 'action', name: 'action', orderable: false, searchable: false}
            ]
        });
    });

    afterDeleteSuccess = function (response) {
        if(typeof response.error != 'undefined') {
            toastr["error"](response.error, "{!! trans('dashboard.error') !!}");
        } else {
            toastr["success"]("{!! trans('dashboard.role_deleted_success_msg') !!}", "{!! trans('dashboard.success') !!}");
        }
        // Redraw grid after success
        if($dataTable !== null) {
            $dataTable.draw();
        }
    };
    afterDeleteError = function () {
        toastr["error"]("{!! trans('dashboard.role_deleted_error_msg') !!}", "{!! trans('dashboard.error') !!}");
    }
</script>
@endsection