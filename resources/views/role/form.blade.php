@extends('layouts.default')

@section('header')
<div class="row">
    <div class="col-lg-12">
        <h2>{{ trans('dashboard.user_roles') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ action('DashboardController@index') }}"><span class="nav-label">{{ trans('dashboard.dashboard') }}</span></a>
            </li>
            <li>
                <a href="{{ action('RoleController@getIndex') }}"><span class="nav-label">{{ trans('dashboard.user_roles') }}</span></a>
            </li>
            <li class="active">
                <strong>{{ isset($role) ? trans('dashboard.update_role') : trans('dashboard.add_role') }}</strong>
            </li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ action('RoleController@postSave') }}">
                    {!! csrf_field() !!}
                    {{ Form::hidden('id', isset($role) ? $role->id : '') }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {{Form::label('name', trans('dashboard.name'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('name',old('name', isset($role) ? $role->name : ''),['class' => 'form-control'])}}

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        {{Form::label('permissions', trans('dashboard.permissions'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            <ul class="list-unstyled file-list">
                                @foreach($permissions as $permission)
                                    <li>
                                        <div class="checkbox checkbox-info" >
                                            <input id="permission-{{ $permission->id }}"
                                                   name="permission[]"
                                                    {{ (isset($role) && isset($rolePermissions) && in_array($permission->id, $rolePermissions)) ? 'checked="checked"' : '' }}
                                                    type="checkbox" value="{{ $permission->id }}" />
                                            <label for="permission-{{ $permission->id }}">
                                                {{ $permission->display_name }}
                                            </label>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn">
                                <i class="fa fa-btn fa-check-square-o"></i> {{ trans('dashboard.save') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
<script src="{{ asset('packages/inspinia/js/plugins/iCheck/icheck.min.js') }}"></script>
<script>

</script>
@endsection