@extends('layouts.default')

@section('styles')
    <link href="{{ asset('packages/inspinia/css/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">
    <link href="{{ asset('packages/inspinia/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <style>
        .flot-tooltip {
            background: #18A689;
        }
        #legendContainer {
            background-color: #fff;
            padding: 2px;
            margin-bottom: 8px;
            border-radius: 3px 3px 3px 3px;
            border: 1px solid #E6E6E6;
            display: inline-block;
            margin: 0 auto;
        }
        .form-group.ios-version{
            margin: 10px 15px;
        }

        .top-buffer{
            margin-top: 20px;;
        }
        .form-group.device-model {
            margin: 10px 10px 10px 15px;
        }
        #flotcontainer {
            width: 600px;
            height: 200px;
            text-align: left;
        }
        .legendColorBox{
          padding-lef: 1px!important;
        }
    </style>
@endsection

@section('header')
<div class="row">
    <div class="col-lg-12">
        <h2>{{ trans('dashboard.dashboard') }}</h2>
        <ol class="breadcrumb">
            <li class="active">
                <strong>{{ trans('dashboard.dashboard') }}</strong>
            </li>
        </ol>
    </div>
</div>
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="ibox float-e-margins">
                <div class="ibox-title">
                    <div class="row">

                        <h3 class="col-md-12 font-bold no-margins">{{ trans('dashboard.event_flot') }} {{  date('M') }} - {{ date('Y') }}  </h3>
                    </div>
                </div>

                <div class="flot-chart dashboard-chart">
                    <div id="legendContainer"></div>
                    <div class="flot-chart-content" id="flot-dashboard-chart"></div>
                </div>

                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="row">
                                <h2 class="col-md-12">{{ trans('dashboard.logs') }}</h2>
                                <div class="panel-body">

                                    <form role="form" class="form-inline" id="search-form" method="POST">
                                        <div class="row">
                                        <div class="form-group col-md-3">
                                            <label for="name">{{ trans('dashboard.customer_name') }} : </label>
                                            {!! Form::select('customer_name', $customer_name, '', ['class' => 'form-control customer-name', 'placeholder' => 'Select Customer']); !!}
                                        </div>

                                        <div class="form-group event col-sm-2">
                                            <label for="name">{{ trans('dashboard.event') }} : </label>
                                            <select  data-placeholder="Select an event" class="form-control" name="event">
                                                <option value=""></option>
                                                <option value="Login">Login</option>
                                                <option value="Offline">Offline</option>
                                                <option value="Open">Open</option>
                                                <option value="Buffer Low">Buffer Low</option>

                                            </select>
                                        </div>

                                        <div class="form-group col-md-3">
                                            <label for="name">{{ trans('dashboard.start_date') }} : </label>

                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text"  id="start_date" name="start_date" class="form-control" value="">
                                            </div>


                                        </div>
                                        <div class="form-group col-md-3">
                                            <label for="email">{{ trans('dashboard.end_date') }} : </label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text"  id="end_date" name="end_date" class="form-control" value="">
                                            </div>

                                        </div>
                                         </div>
                                        <div class="row top-buffer">
                                            <div class="form-group col-md-3">
                                                <label for="email">{{ trans('dashboard.device_model') }} : </label>
                                                {!! Form::select('device_model', $device_model, '', ['class' => 'form-control customer-name', 'placeholder' => 'Select Device Model']); !!}
                                            </div>

                                            <div class="form-group col-md-3">
                                                <label for="email">{{ trans('dashboard.ios_version') }} : </label>
                                                <div class="input-group">
                                                    {!! Form::select('ios_version', $ios_version, '', ['class' => 'form-control customer-name', 'placeholder' => 'Select Ios Version']); !!}
                                                </div>

                                            </div>

                                            <div class="unique-data-show col-md-2">
                                                <label>Unique Records :  </label>
                                                 {!! Form::checkbox('unique_data', 'Yes'); !!}
                                            </div>
                                            <div class="col-md-3">
                                                <button class="btn btn-primary form_button" id="dateSearch" type="submit">Search</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>



                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" id="data-table">
                                    <thead>
                                    <tr>
                                        <th>{{ trans('dashboard.customer_name') }}</th>
                                        <th>{{ trans('dashboard.event') }}</th>
                                        <th>{{ trans('dashboard.event_date') }}</th>
                                        <th>{{ trans('dashboard.device_model') }}</th>
                                        <th>{{ trans('dashboard.ios_version') }}</th>
                                        <th>{{ trans('dashboard.device_detail') }}</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="ibox-content">
                    <div class="row">
                        <div class="col-lg-12">

                            <div class="row">
                                <h2 class="col-md-12">{{ trans('dashboard.removal_request') }}</h2>
                                <div class="panel-body">

                                    <form role="form" class="form-inline" id="search-form2" method="POST">
                                        <div class="row">
                                            <div class="form-group col-md-3">
                                                <label for="name">{{ trans('dashboard.customer_name') }} : </label>
                                                {!! Form::select('customer_id', $customer_name, '', ['class' => 'form-control customer-name', 'placeholder' => 'Select Customer']); !!}
                                            </div>

                                            <div class="form-group col-md-3">
                                                <label for="name">{{ trans('dashboard.stream_title') }} : </label>
                                                {!! Form::select('stream_id', $streams, '', ['class' => 'form-control customer-name', 'placeholder' => 'Select Stream']); !!}
                                            </div>


                                            <div class="form-group event col-sm-2">
                                                <label for="name">{{ trans('dashboard.type') }} : </label>
                                                <select  data-placeholder="Select an event" class="form-control" name="type">
                                                    <option value=""></option>
                                                    <option value="removal">Removal</option>
                                                    <option value="more">More</option>

                                                </select>
                                            </div>


                                            <div class="col-md-3">
                                                <button class="btn btn-primary form_button" id="dateSearch" type="submit">Search</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>



                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover dataTables-example" id="data-table2">
                                    <thead>
                                    <tr>
                                        <th>{{ trans('dashboard.customer_name') }}</th>
                                        <th>{{ trans('dashboard.stream_title') }}</th>
                                        <th>{{ trans('dashboard.meta_data') }}</th>
                                        <th>{{ trans('dashboard.type') }}</th>
                                    </tr>
                                    </thead>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
    <script src="{{ asset('packages/inspinia/js/plugins/dataTables/datatables.min.js') }}"></script>
    <script src="{{ asset('packages/inspinia/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('packages/inspinia/js/plugins/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('packages/inspinia/js/plugins/flot/jquery.flot.tooltip.min.js') }}"></script>
    <script src="{{ asset('packages/inspinia/js/plugins/flot/jquery.flot.spline.js') }}"></script>
    <script src="{{ asset('packages/inspinia/js/plugins/flot/jquery.flot.time.js') }}"></script>
    <script src="{{ asset('packages/inspinia/js/plugins/flot/jquery.flot.symbol.js') }}"></script>
    <script src="{{ asset('packages/inspinia/js/plugins/flot/jquery.flot.axislabels.js') }}"></script>


    <script type="text/javascript">
        $(function() {
            var dataTable = $('#data-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{!! action("DashboardController@postAnyData") !!}',
                    data: function (d) {

                        d.customer_name = $('select[name=customer_name]').val();

                        d.start_date = $('input[name=start_date]').val();
                        d.end_date = $('input[name=end_date]').val();
                        d.event = $('select[name=event]').val();
                        d.device_model = $('select[name=device_model]').val();
                        d.ios_version = $('select[name=ios_version]').val();
                        d.unique_data = $('input[name=unique_data]:checked').val();;
                    //    d.device_detail = $('select[name=device_detail]').val();
                    },
                    type : 'POST',
                    headers: {"X-CSRF-TOKEN": $('meta[name=csrf-token]').attr('content')}
                },
                    oLanguage: {
                        sLengthMenu: "_MENU_",
                        sSearch: ""
                    },
                columns: [
                    { data: 'customer_id', name: 'customers.name' },
                    { data: 'event', name: 'logs.event' },
                    { data: 'event_datetime', name: 'logs.event_datetime' },
                    { data: 'device_model', name: 'logs.device_model' },
                    { data: 'ios_version', name: 'logs.ios_version' },
                    { data: 'device_detail', name: 'logs.device_detail', orderable: false, searchable: true }
                ]
            });

            $('#search-form').on('submit', function(e) {
                dataTable.draw();
                e.preventDefault();
            });

            var dataTable2 = $('#data-table2').DataTable({
                processing: true,
                serverSide: true,
                ajax: {
                    url: '{!! action("DashboardController@postAnyRequestData") !!}',
                    data: function (d) {

                        d.customer_id = $('select[name=customer_id]').val();
                        d.stream_id = $('select[name=stream_id]').val();
                        d.type = $('select[name=type]').val();
                        //    d.device_detail = $('select[name=device_detail]').val();
                    },
                    type : 'POST',
                    headers: {"X-CSRF-TOKEN": $('meta[name=csrf-token]').attr('content')}
                },
                oLanguage: {
                    sLengthMenu: "_MENU_",
                    sSearch: ""
                },
                columns: [
                    { data: 'customer_id', name: 'customer_id' },
                    { data: 'stream_id', name: 'stream_id' },
                    { data: 'meta_data', name: 'meta_data' },
                    { data: 'type', name: 'type', orderable: false, searchable: true },
                ]
            });

            $('#search-form2').on('submit', function(e) {
                dataTable2.draw();
                e.preventDefault();
            });
        });


        $('.input-group.date').datepicker({
            todayBtn: "linked",
            keyboardNavigation: false,
            forceParse: false,
            calendarWeeks: true,
            autoclose: true,
            format: 'yyyy-mm-dd'
        });
        var offline = {!! json_encode($ol) !!};
        var login = {!! json_encode($login) !!};
        var buffer_low = {!! json_encode($bl) !!};
        var open = {!! json_encode($open) !!};

        var data1 = [
            { label: "Offline", data: offline, color: '#E96E67 '},
            { label: "Login", data: login, color: '#f1ce0c' },
            { label: "Buffer", data: buffer_low, color: '#34044d' },
            { label: "Open", data: open, color: '#17a084' }
        ];

        $("#flot-dashboard-chart").length && $.plot($("#flot-dashboard-chart"),
                    data1
                ,
                {
                    series: {
                        lines: {
                            show: false,
                            fill: true
                        },
                        splines: {
                            show: true,
                            tension: 0.4,
                            lineWidth: 1,
                            fill: 0.4
                        },
                        points: {
                            radius: 2,
                            show: true
                        },
                        shadowSize: 2
                    },
                    grid: {
                        hoverable: true,
                        clickable: true,
                        tickColor: "#d5d5d5",
                        borderWidth: 1,
                        color: '#d5d5d5',
                        axisMargin : 40

                    },
                    colors: ["#1ab394", "#464f88"],
                    xaxis:{
                        ticks: 30,
                        tickSize: 3,
                        tickDecimals: 0,
                        axisLabel: 'foo'

                    },
                    yaxis: {
                        ticks: 5,
                        max : 10,
                        autoscaleMargin : 20

                    },
                    legend: {
                        show: true,
                        noColumns: 0, // number of colums in legend table
                        container: $("#legendContainer") // container (as jQuery object) to put legend in, null means default on top of graph

                    },
                    tooltip: {
                        show : true,
                        content: "%s | Date: %x | Event: %y",
                        cssClass : 'flot-tooltip'
                    },
                    axisLabels: {
                        show: true
                    },
                    xaxes: [{
                        axisLabel: 'Days'
                    }],
                    yaxes: [{
                        position: 'left',
                        axisLabel: 'Events'
                    }, {
                        position: 'right',
                        axisLabel: 'bleem'
                    }]
                }
        );

        function gd(year, month, day) {
            return new Date(year, month - 1, day).getTime();
        }
    </script>
@endsection