@extends('layouts.default')

@section('header')
<div class="row">
    <div class="col-lg-12">
        <h2>{{ trans('dashboard.settings') }}</h2>
        <ol class="breadcrumb">
            <li>
                <a href="{{ action('DashboardController@index') }}"><span class="nav-label">{{ trans('dashboard.dashboard') }}</span></a>
            </li>
            <li class="active">
                <strong>{{ trans('dashboard.settings') }}</strong>
            </li>
        </ol>
    </div>
</div>
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="ibox float-e-margins">
            <div class="ibox-content">
                <form class="form-horizontal" role="form" method="POST" action="{{ action('AccountController@postSave') }}">
                    {!! csrf_field() !!}
                    {{ Form::hidden('id', isset($user) ? $user->id : '') }}
                    {{ Form::hidden('role_id', isset($user) ? $user->role_id : '') }}

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        {{Form::label('name', trans('dashboard.name'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::text('name',old('first_name', isset($user) ? $user->name : ''),['class' => 'form-control'])}}

                            @if ($errors->has('name'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('name') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        {{Form::label('email', trans('dashboard.email'),['class' => 'col-md-4 control-label'])}}

                        <div class="col-md-6">
                            {{Form::email('email',old('email', isset($user) ? $user->email : ''),['class' => 'form-control'])}}

                            @if ($errors->has('email'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('email') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('new_password') ? ' has-error' : '' }}">
                        {{Form::label('new_password', trans('dashboard.new_password'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::password('new_password', ['class' => 'form-control'])}}

                            @if ($errors->has('new_password'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('new_password') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }}">
                        {{Form::label('password', trans('dashboard.confirm_password'),['class' => 'col-md-4 control-label'])}}
                        <div class="col-md-6">
                            {{Form::password('password_confirmation', ['class' => 'form-control'])}}

                            @if ($errors->has('password_confirmation'))
                                <span class="help-block">
                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-6 col-md-offset-4">
                            <button type="submit" class="btn">
                                <i class="fa fa-btn fa-check-square-o"></i> {{ trans('dashboard.save') }}
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_scripts')
<script>

</script>
@endsection