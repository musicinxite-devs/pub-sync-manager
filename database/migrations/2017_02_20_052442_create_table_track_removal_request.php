<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTrackRemovalRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('track_removal_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stream_id')->nullable()->unsigned();
            $table->integer('customer_id')->nullable()->unsigned();
            $table->string('meta_data');
            $table->enum('type', ['more', 'removal']);
            $table->timestamps();
            $table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
            $table->foreign('stream_id')->references('id')->on('streams')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('track_removal_requests');
    }
}
