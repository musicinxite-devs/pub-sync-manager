<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SetDefaultDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('customers', function ($table) {
            $table->date('subscription_start_date')->default('0000-00-00')->change();
            $table->date('subscription_end_date')->default('0000-00-00')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('customers', function ($table) {
            $table->date('subscription_start_date')->nullable()->change();
            $table->date('subscription_end_date')->nullable()->change();
        });
    }

}
