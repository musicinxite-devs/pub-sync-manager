<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeFieldOrderInLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('logs', function ($table) {
            $table->string('device_model')->after('device_detail')->change();
            $table->string('ios_version')->after('device_model')->change();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('logs', function ($table) {
            $table->string('device_model')->after('updated_at')->change();
            $table->string('ios_version')->after('device_model')->change();
        });
    }
}
