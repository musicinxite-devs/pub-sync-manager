<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStreamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('streams', function (Blueprint $table) {
            $table->increments('id');
			$table->string('title');
			$table->string('url');
			$table->enum('status', ['active', 'inactive']);
			$table->integer('library_id')->unsigned();
            $table->timestamps();
			$table->foreign('library_id')->references('id')->on('libraries')->onDelete('cascade');
        });
		Schema::create('customer_stream', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('customer_id')->unsigned();
			$table->integer('stream_id')->unsigned();
			
			$table->foreign('customer_id')->references('id')->on('customers')->onDelete('cascade');
			$table->foreign('stream_id')->references('id')->on('streams')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('customer_stream');
		Schema::drop('streams');
    }
}
