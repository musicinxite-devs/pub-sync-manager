<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ChangeLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('logs', function(Blueprint $table) {
            $table->dropColumn('library_id');
            $table->integer('customer_id')->unsigned()->after('id');
            $table->dateTime('event_datetime')->after('event');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('logs', function(Blueprint $table) {
            $table->dropColumn('customer_id');
            $table->dropColumn('event_datetime');
            $table->integer('library_id')->unsigned()->after('id');
        });
    }
}
