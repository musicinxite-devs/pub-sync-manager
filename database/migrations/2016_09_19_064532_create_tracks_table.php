<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTracksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tracks', function (Blueprint $table) {
            $table->increments('id');
			$table->string('title');
			$table->string('file_path');
            $table->timestamps();
        });
		
		Schema::create('library_track', function (Blueprint $table) {
            $table->increments('id');
			
			$table->integer('library_id')->unsigned();
			$table->integer('track_id')->unsigned();
			
			$table->foreign('library_id')->references('id')->on('libraries')->onDelete('cascade');
			$table->foreign('track_id')->references('id')->on('tracks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('library_track');
		Schema::drop('tracks');
    }
}
