<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Role;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = config('roles');
        foreach($roles as $role_id => $roleConfig) {
            if(isset($roleConfig['default-users']) && is_array($roleConfig['default-users'])) {
                foreach($roleConfig['default-users'] as $email => $defaultUser) {
                    $user = User::firstOrCreate([
                        'email' => $email,
                        'role_id' => $role_id
                    ]);
                    $user->name = $defaultUser['name'];
                    $user->password = bcrypt($defaultUser['password']);
                    $user->remember_token = "";
                    $user->is_system_user = true;
                    $user->save();
                }
            }
        }
    }
}
