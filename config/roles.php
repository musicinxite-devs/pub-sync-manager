<?php
return [
    1 => [
        'role' => 'Super Admin',
        'default-users' => [
            "admin@avdevs.com" => [
                "name" => "Admin",
                "password" => "avds@123"
            ]
        ],
        'permissions' => [
            'admin_dashboard',
            'user_management',
            'view_roles',
            'add_role',
            'edit_role',
            'delete_role',
            'view_users',
            'add_user',
            'edit_user',
            'delete_user',
            'my_account',
			'view_customers',
			'add_customers',
			'edit_customers',
			'delete_customers',
            'view_libraries',
            'add_library',
            'edit_library',
            'delete_library',
            'view_tracks',
            'add_track',
            'edit_track',
            'delete_track',
            'view_streams',
            'add_stream',
            'edit_stream',
            'delete_stream',
			'view_logs'
        ]
    ],
    2 => [
        'role' => 'Customer',
        'permissions' => [
            'admin_dashboard',
            'my_account',
        ]
    ],
    3 => [
        'role' => 'Guest',
        'permissions' => [
        ]
    ]
];