<?php

return array (
    // Navigation configuration for Admin Menu
    'admin' => array (
        array (
            // Title for the link
            'title' => "dashboard.dynamic.home",

            // This is the name of permission as used in permissions in DB
            // using Confide/Entrust plugin
            'permission' => "admin_dashboard",

            // Set link icon
            'icon' => 'fa fa-home',

            // (Optional)
            // Set is menu visible in navigation or not (Default: true)
            'visible'	=> true,

            /*
             * Option for the Menu Item
             * Accepts String for route or Array
             * Ref: https://github.com/lavary/laravel-menu
             *
             */
            'options' => array (
                // Set route or url for the link
                'action' => "DashboardController@index"
            ),
            'attributes' => array (
                // Set Attributes for the link if required
            ),
            'childrens' => array(
                // Set child menu items
            )
        ),
    ),
    // Navigation for frontend navigation
    'frontend' => array (
        array (
            'title' => "dashboard.dashboard",
            'permission' => "admin_dashboard",
            'icon' => 'fa fa-desktop',
            'options' => array (
                'action' => "DashboardController@index"
            ),
        ),
        array (
            'title' => "dashboard.tracks",
            'permission' => "view_tracks",
            'icon' => 'fa fa-file-audio-o',
            'options' => array (
                'action' => "TrackController@getIndex"
            ),
            'childrens' => array(
                array(
                    'title' => "dashboard.add_track",
                    'permission' => "add_track",
                    'visible'	=> false,
                    'options' => array (
                        'action' => "TrackController@getCreate"
                    )
                ),
                array(
                    'title' => "dashboard.update_track",
                    'permission' => "edit_track",
                    'visible'	=> false,
                    'options' => array (
                        'url' => "track/edit"
                    )
                )
            )
        ),
        array (
            'title' => "dashboard.offline_library",
            'permission' => "view_libraries",
            'icon' => 'fa fa-file-audio-o',
            'options' => array (
                'action' => "LibraryController@getIndex"
            ),
            'childrens' => array(
                array(
                    'title' => "dashboard.add_library",
                    'permission' => "add_library",
                    'visible'	=> false,
                    'options' => array (
                        'action' => "LibraryController@getCreate"
                    )
                ),
                array(
                    'title' => "dashboard.update_library",
                    'permission' => "edit_library",
                    'visible'	=> false,
                    'options' => array (
                        'url' => "library/edit"
                    )
                )
            )
        ),
        array (
            'title' => "dashboard.streams",
            'permission' => "view_streams",
            'icon' => 'fa fa-file-audio-o',
            'options' => array (
                'action' => "StreamController@getIndex"
            ),
            'childrens' => array(
                array(
                    'title' => "dashboard.add_stream",
                    'permission' => "add_stream",
                    'visible'	=> false,
                    'options' => array (
                        'action' => "StreamController@getCreate"
                    )
                ),
                array(
                    'title' => "dashboard.update_stream",
                    'permission' => "edit_stream",
                    'visible'	=> false,
                    'options' => array (
                        'url' => "library/edit"
                    )
                )
            )
        ),
        array (
            'title' => "dashboard.customers",
            'permission' => "view_customers",
            'icon' => 'fa fa-users',
            'options' => array (
                'action' => "CustomerController@getIndex"
            ),
            'childrens' => array(
                array(
                    'title' => "dashboard.add_customer",
                    'permission' => "add_customers",
                    'visible'	=> false,
                    'options' => array (
                        'action' => "CustomerController@getCreate"
                    )
                ),
                array(
                    'title' => "dashboard.update_customer",
                    'permission' => "edit_customers",
                    'visible'	=> false,
                    'options' => array (
                        'url' => "customer/edit"
                    )
                )
            )
        ),
        array (
            'title' => "dashboard.user_management",
            'permission' => "user_management",
            'icon' => 'fa fa-gears',
            'options' => array (
                'url' => "#"
            ),
            'childrens' => array(
                array (
                    'title' => "dashboard.users",
                    'permission' => "view_users",
                    'icon' => 'fa fa-users',
                    'options' => array (
                        'action' => "UserController@getIndex"
                    ),
                    'childrens' => array(
                        array(
                            'title' => "dashboard.create_users",
                            'permission' => "add_user",
                            'visible'	=> false,
                            'options' => array (
                                'action' => "UserController@getCreate"
                            )
                        ),
                        array(
                            'title' => "dashboard.editusers",
                            'permission' => "edit_user",
                            'visible'	=> false,
                            'options' => array (
                                'url' => "user/edit"
                            )
                        )
                    )
                ),
                array (
                    'title' => "dashboard.roles",
                    'permission' => "view_roles",
                    'icon' => 'fa fa-sitemap',
                    'options' => array (
                        'action' => "RoleController@getIndex"
                    ),
                    'childrens' => array(
                        array(
                            'title' => "dashboard.create_role",
                            'permission' => "add_role",
                            'visible'	=> false,
                            'options' => array (
                                'action' => "RoleController@getCreate"
                            )
                        ),
                        array(
                            'title' => "dashboard.edit_role",
                            'permission' => "edit_role",
                            'visible'	=> false,
                            'options' => array (
                                'url' => "role/edit"
                            )
                        )
                    )
                )
            )
        ),
        'account' => [
            'title' => 'dashboard.dynamic.my_account',
            'icon' => 'fa fa-cog',
            'permission' => 'my_account',
            'options' => [
                'action' => "AccountController@getIndex"
            ]
        ]
    )
);