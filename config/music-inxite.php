<?php
return [
    "common" => [
        'storage_temp_dir' => 'app/tmp/',
        'dateformat' => array(
            'js' => 'mm/dd/yyyy',
            'php' => 'm/d/Y'
        )
    ],
];