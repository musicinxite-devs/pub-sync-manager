var $dataTable = null;
var afterDeleteSuccess = function (response) {

};
var afterDeleteError = function () {

};
(function($) {
    // Common Delete function
    $(document).on("click", ".btn-delete", function (e) {
        e.preventDefault();
        var url = $(this).attr("href");
        var id = $(this).data("id");

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: {id: id, _token: csrfToken},
            success: function (response) {
                afterDeleteSuccess(response);
            },
            error: function () {
                afterDeleteError();
            }
        });
    });
})(jQuery);